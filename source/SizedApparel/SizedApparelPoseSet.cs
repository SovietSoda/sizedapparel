﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;
using RimWorld;

namespace SizedApparel
{
    public class SizedApparelPoseSetDef: Def
    {
        public List<SizedApparelPose> poses;
    }
    
    public class SizedApparelPose
    {
        public SizedApparelBodyPartOf targetBodyPart = SizedApparelBodyPartOf.Torso;
        public string poseName;
    }

}
