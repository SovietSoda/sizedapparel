﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using RimWorld;
using rjw;
using Verse;

namespace SizedApparel
{
    [StaticConstructorOnStartup]
    public class ApparelRecorderComp : ThingComp
    {
        public bool testbool = false;
        public ApparelRecorderCompProperties Props => (ApparelRecorderCompProperties)this.props;
        public bool hasUpdateBefore = false;
        public bool hasUpdateBeforeSuccess = false;
        public bool hasGraphicUpdatedBefore = false; // not yet

        public bool needToCheckApparelGraphicRecords = false;
        public bool isDirty = true;

        public bool hasUnsupportedApparel = true;
        public bool havingSex = false;//Obsolete
        public bool hasUpdateForSex = false;//Obsolete


        public List<ApparelGraphicRecord> cachedApparelGraphicRecord = new List<ApparelGraphicRecord>();



        public Hediff breastHediff = null;
        public Hediff vaginaHediff = null;
        public List<Hediff> penisHediffs = null; // RJW can attach multiple penis
        public Hediff anusHediff = null;

        public Hediff udderHediff = null;//RJW 4.6.8: Udder is not partof chest's breast. it attached to torso.

        public float breastSeverity = -1;

        public float breastSeverityCapToDraw = 1000;
        public float BreastSeverityCache = 0;
        //public float BiggestBreastSeverityInAvailableTextures = 0;


        //for breasts animation or something.
        public Vector3? prePositionCache;
        public float? preAngleCache;
        public int? preTickCache;


        public SizedApparelBodyPart bodyPartBreasts;
        public SizedApparelBodyPart bodyPartNipple; //TODO
        public List<SizedApparelBodyPart> bodyPartPenises = new List<SizedApparelBodyPart>();
        public List<SizedApparelBodyPart> bodyPartBalls = new List<SizedApparelBodyPart>();
        public SizedApparelBodyPart bodyPartVagina;
        public SizedApparelBodyPart bodyPartAnus;
        public SizedApparelBodyPart bodyPartBelly;
        public SizedApparelBodyPart bodyPartMuscleOverlay;//TODO
        public SizedApparelBodyPart bodyPartUdder;
        public SizedApparelBodyPart bodyPartPubicHair; //TODO
        public SizedApparelBodyPart bodyPartHips;
        public List<SizedApparelBodyPart> bodyPartThighs = new List<SizedApparelBodyPart>();
        public List<SizedApparelBodyPart> bodyPartHands = new List<SizedApparelBodyPart>();
        public List<SizedApparelBodyPart> bodyPartFeet = new List<SizedApparelBodyPart>();

        public Graphic graphicNaked = null; //original Graphic
        public Graphic graphicRotten = null; //original Graphic

        public Graphic graphicbaseBodyNaked = null;
        public Graphic graphicbaseBodyRotten = null;

        [Obsolete]
        public Graphic graphicBreasts = null;
        [Obsolete]
        public Graphic graphicBreasts_horny = null;
        [Obsolete]
        public Graphic graphicVagina = null;
        [Obsolete]
        public Graphic graphicVagina_horny = null;
        [Obsolete]
        public Graphic graphicAnus = null;
        [Obsolete]
        public Graphic graphicAnus_horny = null;
        [Obsolete]
        public List<Graphic> graphicPenises = new List<Graphic>();
        [Obsolete]
        public List<Graphic> graphicBallsOfPenises = new List<Graphic>();
        [Obsolete]
        public Graphic graphicBelly = null;
        [Obsolete]
        public Graphic graphicUdder = null; //RJW 4.6.8: Udder is not partof chest's breast. it attached to torso.
        [Obsolete]
        public Graphic graphicUdder_horny = null;
        [Obsolete]
        public List<Graphic> graphicPenises_horny = new List<Graphic>();
        [Obsolete]
        public List<Graphic> graphicBalls_horny = new List<Graphic>();//Does it need?

        //size uncapped breasts graphics.
        [Obsolete]
        public Graphic graphicBreastsNude = null;
        [Obsolete]
        public Graphic graphicBreastsNude_horny = null;

        private bool drawingBreastsAddon = false;


        public string customPose = null;

        public bool canDrawBreasts = false;
        public bool canDrawPenis = false;
        public bool canDrawVaginaAndAnus = false;
        public bool canDrawTorsoParts = false; //belly and udder

        public override void Initialize(CompProperties props)
        {
            base.Initialize(props);
            Pawn p = parent as Pawn;
            bodyPartBreasts = new SizedApparelBodyPart(p, "Breasts", SizedApparelBodyPartOf.Breasts, "Breasts", true, false);
            bodyPartBreasts.SetDepthOffsets(0.0113f, 0.001f, 0.0113f, 0.0113f);

            bodyPartUdder = new SizedApparelBodyPart(p, "Udder", SizedApparelBodyPartOf.Breasts, "UdderBreasts", true, false);
            bodyPartUdder.SetDepthOffsets(0.0112f, 0.0005f, 0.0112f, 0.0112f);

            bodyPartVagina = new SizedApparelBodyPart(p, "Vagina", SizedApparelBodyPartOf.Vagina, "Vagina", false, false);
            bodyPartVagina.SetDepthOffsets(0.0088f, 0.0100f, 0.0088f, 0.0088f);

            bodyPartAnus = new SizedApparelBodyPart(p, "Anus", SizedApparelBodyPartOf.Anus, "Anus", false, false);
            bodyPartAnus.SetDepthOffsets(0.0093f, 0.0105f, 0.0093f, 0.0093f);

            bodyPartBelly = new SizedApparelBodyPart(p, "Belly", SizedApparelBodyPartOf.Belly, "BellyBulge", false, false);
            bodyPartBelly.SetDepthOffsets(0.0098f, 0.0002f, 0.0098f, 0.0098f);

        }

        public override void PostExposeData()
        {
            base.PostExposeData();
            //Scribe_Values.Look<>(); //TODO: save pubic hair data
        }



        /*
        public override void CompTick()
        {
            base.CompTick();


        }

        public override void CompTickRare()
        {
            base.CompTickRare();
        }*/

        string[] testPose = { null, "JiggleUp", "JiggleCenter" };
        string[] testPose2 = { null, "PenisDown" };

        public void DrawUpdate(Vector3 rootLoc, float angle) // call this in DrawPawnBody
        {
            //int IdTick = parent.thingIDNumber * 20; //hint from yayo animation mod
            if (SizedApparelSettings.autoJiggleBreasts)
            {
                Vector3 velocity;
                if (this.prePositionCache != null)
                    velocity = this.prePositionCache.Value - rootLoc;
                else
                    velocity = Vector3.zero;

                

                float rotation;
                if (this.preAngleCache != null)
                    rotation = this.preAngleCache.Value - angle;
                else
                    rotation = 0;

                int tick;
                if (this.preTickCache != null)
                    tick = Find.TickManager.TicksGame - this.preTickCache.Value;
                else
                    tick = 1;



                this.prePositionCache = rootLoc;
                this.preAngleCache = angle;
                this.preTickCache = Find.TickManager.TicksGame;

            }

            SetPoseFromPoseSet(testPose2.RandomElement());
        }


        public bool isApparelGraphicRecordChanged()
        {
            Pawn pawn = parent as Pawn;
            if (pawn == null)
                return false;

            var apparelGraphics = pawn.Drawer?.renderer?.graphics?.apparelGraphics;
            if (apparelGraphics == null)
                return false;

            //return false; //since 1.3 broken. force to return false;
            if (!apparelGraphics.SequenceEqual(cachedApparelGraphicRecord))
            {
                if (SizedApparelSettings.Debug)
                    Log.Message("[SizedApparel]:" + pawn + "'s apparel Record Changed! need to updating comp");
                return true;
            }


            return false;
        }


        //not working
        public override void PostPostMake()
        {
            /*
            Pawn pawn;
            try
            {
                pawn = (Pawn)parent;
                if (pawn != null)
                {
                    if (!pawn.RaceProps.Humanlike)
                        return;
                    pawn.Drawer.renderer.graphics.ResolveAllGraphics();
                    if (SizedApparelSettings.Debug)
                        Log.Message("[SizedApparels] Component Inint And Resolve all Graphics for "+ pawn.Name);
                }
            }
            catch
            {

            }
            */
        }
        public void ClearHediffs()
        {
            breastHediff = null;
            vaginaHediff = null;
            if(penisHediffs != null)
                penisHediffs.Clear();
            anusHediff = null;

            udderHediff = null;
        }
        public void ClearBreastCacheValue()
        {
            breastHediff = null;
            breastSeverity = -1;
            breastSeverityCapToDraw = 1000;
            BreastSeverityCache = 0;
        }
        public void ClearPenisCacheValue()
        {
            //TODO
        }
        public void ClearCanDraw()
        {
            canDrawBreasts = false;
            canDrawPenis = false;
            canDrawTorsoParts = false;
            canDrawVaginaAndAnus = false;
        }
        public void ClearGraphics()
        {
            hasGraphicUpdatedBefore = false;
            graphicbaseBodyNaked = null;
            graphicbaseBodyRotten = null;

            //Clear BodyParts
            bodyPartBreasts.Clear();
            bodyPartAnus.Clear();
            bodyPartBelly.Clear();
            bodyPartVagina.Clear();

            graphicBreasts = null;
            graphicVagina = null;
            graphicAnus = null;
            if (graphicPenises == null)
                graphicPenises = new List<Graphic>();
            else
                graphicPenises.Clear();
            if (graphicBallsOfPenises == null)
                graphicBallsOfPenises = new List<Graphic>();
            else
                graphicBallsOfPenises.Clear();
            graphicBelly = null;

            graphicUdder = null;
            graphicUdder_horny = null;

            graphicBreasts_horny = null;
            graphicVagina_horny = null;
            graphicAnus_horny = null;
            if (graphicPenises_horny == null)
                graphicPenises_horny = new List<Graphic>();
            else
                graphicPenises_horny.Clear();
            if (graphicBalls_horny == null)
                graphicBalls_horny = new List<Graphic>();
            else
                graphicBalls_horny.Clear();
        }
        public void ClearAll(bool clearGraphics = true)
        {
            ClearBreastCacheValue();
            if(clearGraphics)
                ClearGraphics();
            ClearHediffs();
            ClearCanDraw();
            hasUnsupportedApparel = false;
            hasUpdateBefore = false;
            hasUpdateBeforeSuccess = false;
            needToCheckApparelGraphicRecords = false;
        }

        public void SetDirty()
        {
            this.isDirty = true;
        }

        public void UpdateIfDirty()
        {

        }
  
        public void UpdateBodyGraphics() { 
}

        public void Update(bool cache = true, bool fromGraphicRecord = true, bool updateGraphics = true, bool CheckApparel = true)
        {
            isDirty = false;
            bool flag = fromGraphicRecord;
            needToCheckApparelGraphicRecords = false;
            //flag = false; //TODO:fromGraphicRecord is forced not to do for a while. it will update later
            hasUpdateBefore = true;
            Pawn pawn = parent as Pawn;
            if (pawn == null)
                return;
            if (SizedApparelSettings.Debug)
                Log.Message("[SizedApparel] Updating Component of " + pawn.Name);
            SizedApparelUtility.GetBreastSeverity(pawn, out breastSeverity, out breastHediff);
            if (breastHediff != null)
            {
                BreastSeverityCache = 1000;
                if (CheckApparel)
                    hasUnsupportedApparel = SizedApparelUtility.hasUnSupportedApparelFromWornData(pawn, breastSeverity, breastHediff, true, flag);
                else
                    hasUnsupportedApparel = false;
                //float validBreastTextureSeverity = SizedApparelUtility.GetBreastSeverityValidTextures(pawn, breastHediff);
                if (hasUnsupportedApparel)
                    BreastSeverityCache = breastSeverity;
                //float validBreastTextureSeverity = SizedApparelUtility.GetBreastSeverityValidTextures(pawn, breastHediff);
                var key = new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, pawn.story?.bodyType?.defName, breastHediff.def.defName, "Breasts", SizedApparelUtility.BreastSeverityInt(breastHediff.Severity));
                float validBreastTextureSeverity = SizedApparelUtility.BreastSizeIndexToSeverity(SizedApparelsDatabase.GetSupportedBodyPartPath(key,true,"Breasts","Breasts").size);
                if (validBreastTextureSeverity < -5 && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                    validBreastTextureSeverity = SizedApparelUtility.GetBreastSeverityValidTextures(pawn, breastHediff, "Humanlike");
                BreastSeverityCache = Math.Min(BreastSeverityCache, validBreastTextureSeverity);
                if (SizedApparelSettings.Debug)
                    Log.Message("[Sized Apparel] cached Severity : " + BreastSeverityCache);
            }
            //penisHediff = Genital_Helper.get_PartsHediffList(pawn, Genital_Helper.get_genitalsBPR(pawn)).FirstOrDefault((Hediff h) => h.def.defName.ToLower().Contains("penis"));

            //since rjw race support's part name are too various, not handling it.

            BodyPartRecord genitalPart = Genital_Helper.get_genitalsBPR(pawn);
            if (genitalPart != null)
            {
                var genitalList = Genital_Helper.get_PartsHediffList(pawn, genitalPart);
                if (!genitalList.NullOrEmpty())
                {
                    penisHediffs = genitalList.FindAll((Hediff h) => SizedApparelUtility.isPenis(h.def.defName));
                    vaginaHediff = genitalList.FirstOrDefault((Hediff h) => SizedApparelUtility.isVagina(h.def.defName));
                }

            }
            var anusList = Genital_Helper.get_PartsHediffList(pawn, Genital_Helper.get_anusBPR(pawn));
            if(!anusList.NullOrEmpty())
                anusHediff = anusList.FirstOrDefault((Hediff h) => h.def.defName.ToLower().Contains("anus"));
            //udderHediff = Genital_Helper.get_PartsHediffList(pawn, pawn.RaceProps.body.AllParts.Find((BodyPartRecord bpr) => bpr.def.defName == "Torso")).FirstOrDefault((Hediff h) => h.def.defName.ToLower().Contains("anus")); //not yet supported

            udderHediff = pawn.health?.hediffSet?.hediffs?.FirstOrDefault((Hediff h) => h.def.defName.ToLower().Contains("udder"));

            var pawnRenderer = pawn.Drawer?.renderer?.graphics;

            if (updateGraphics)
            {
                ClearGraphics();

                if (pawnRenderer != null)
                {
                    //pawnRenderer.ResolveAllGraphics();
                    string nakedGraphicPath = null;
                    if (pawnRenderer.nakedGraphic != null)
                    {
                        //path = path.Insert(Math.Max(path.LastIndexOf('/'), 0), "/CustomPose/"+ customPose);

                        if (customPose != null)
                            nakedGraphicPath = nakedGraphicPath.Insert(Math.Max(nakedGraphicPath.LastIndexOf('/'), 0), "/CustomPose/" + customPose);
                        else
                            nakedGraphicPath = pawnRenderer.nakedGraphic.path;

                        if (!nakedGraphicPath.Contains("_BaseBody"))
                        {
                            if (ContentFinder<Texture2D>.Get((nakedGraphicPath + "_BaseBody" + "_south"), false) != null)
                            {
                                graphicNaked = pawnRenderer.nakedGraphic;
                                Shader shader = pawnRenderer.nakedGraphic.Shader;
                                //if (!ShaderUtility.SupportsMaskTex(shader))
                                //    shader = ShaderDatabase.CutoutSkinOverlay;
                                graphicbaseBodyNaked = GraphicDatabase.Get<Graphic_Multi>(nakedGraphicPath + "_BaseBody", shader, pawnRenderer.nakedGraphic.drawSize, pawnRenderer.nakedGraphic.color, pawnRenderer.nakedGraphic.colorTwo, pawnRenderer.nakedGraphic.data);
                                //pawnRenderer.nakedGraphic = graphicbaseBodyNaked;
                            }
                            else if(customPose != null)
                            {
                                nakedGraphicPath = pawnRenderer.nakedGraphic.path;
                                if (ContentFinder<Texture2D>.Get((nakedGraphicPath + "_BaseBody" + "_south"), false) != null)
                                {
                                    graphicNaked = pawnRenderer.nakedGraphic;
                                    Shader shader = pawnRenderer.nakedGraphic.Shader;
                                    //if (!ShaderUtility.SupportsMaskTex(shader))
                                    //    shader = ShaderDatabase.CutoutSkinOverlay;
                                    graphicbaseBodyNaked = GraphicDatabase.Get<Graphic_Multi>(nakedGraphicPath + "_BaseBody", shader, pawnRenderer.nakedGraphic.drawSize, pawnRenderer.nakedGraphic.color, pawnRenderer.nakedGraphic.colorTwo, pawnRenderer.nakedGraphic.data);
                                    //pawnRenderer.nakedGraphic = graphicbaseBodyNaked;
                                }
                                else
                                {
                                    if (SizedApparelSettings.Debug)
                                        Log.Warning("[SizedApparel] Missing BaseBodyTexture for naked Graphic: " + nakedGraphicPath + "_BaseBody" + "_south");
                                }

                            }
                        }
                        else
                            graphicbaseBodyNaked = pawnRenderer.nakedGraphic;

                    }

                    string rottingGraphicPath = null;
                    if (pawnRenderer.rottingGraphic != null)
                    {
                        if (customPose != null)
                            rottingGraphicPath = rottingGraphicPath.Insert(Math.Max(rottingGraphicPath.LastIndexOf('/'), 0), "/CustomPose/" + customPose);
                        else
                            rottingGraphicPath = pawnRenderer.rottingGraphic.path;

                        if (!rottingGraphicPath.Contains("_BaseBody"))
                        {
                            if (ContentFinder<Texture2D>.Get((rottingGraphicPath + "_BaseBody" + "_south"), false) != null)
                            {
                                graphicRotten = pawnRenderer.rottingGraphic;
                                Shader shader = pawnRenderer.rottingGraphic.Shader;
                                //if (!ShaderUtility.SupportsMaskTex(shader))
                                //    shader = ShaderDatabase.CutoutSkinOverlay;
                                graphicbaseBodyRotten = GraphicDatabase.Get<Graphic_Multi>(rottingGraphicPath + "_BaseBody", shader, pawnRenderer.rottingGraphic.drawSize, pawnRenderer.rottingGraphic.color, pawnRenderer.rottingGraphic.colorTwo, pawnRenderer.rottingGraphic.data);
                                //pawnRenderer.rottingGraphic = graphicbaseBodyRotten;
                            }
                            else if (customPose != null)
                            {
                                rottingGraphicPath = pawnRenderer.rottingGraphic.path;
                                if (ContentFinder<Texture2D>.Get((rottingGraphicPath + "_BaseBody" + "_south"), false) != null)
                                {
                                    graphicRotten = pawnRenderer.rottingGraphic;
                                    Shader shader = pawnRenderer.rottingGraphic.Shader;
                                    //if (!ShaderUtility.SupportsMaskTex(shader))
                                    //    shader = ShaderDatabase.CutoutSkinOverlay;
                                    graphicbaseBodyRotten = GraphicDatabase.Get<Graphic_Multi>(rottingGraphicPath + "_BaseBody", shader, pawnRenderer.rottingGraphic.drawSize, pawnRenderer.rottingGraphic.color, pawnRenderer.rottingGraphic.colorTwo, pawnRenderer.rottingGraphic.data);
                                    //pawnRenderer.rottingGraphic = graphicbaseBodyRotten;
                                }
                                else
                                {
                                    if (SizedApparelSettings.Debug)
                                        Log.Warning("[SizedApparel] Missing BaseBodyTexture for naked Graphic: " + rottingGraphicPath + "_BaseBody" + "_south");
                                }

                            }
                        }
                        else
                            graphicbaseBodyRotten = pawnRenderer.rottingGraphic;

                    }

                    /*
                    //Try to find sized Body if it's valid
                    int offset = 9999;
                    int currentBreastSizeIndex = 0;
                    float currentBreastSeverity = 0;
                    bool validNakedTexture = false;
                    bool validRottingTexture = false;
                    Graphic nakedGraphic;
                    Graphic rottingGraphic;
                    while (offset < SizedApparelUtility.size.Length)
                    {
                        string breastSeverityStringCache = SizedApparelUtility.BreastSeverityString(breastSeverity, offset, true, ref currentBreastSizeIndex, ref currentBreastSeverity);
                        //search bigger
                        //SizedApparelSettings.matchBodyTextureToMinimumApparelSize? currentBreastSizeIndex <= minSupportedBreasSizeIndex:true
                        if (validNakedTexture == false && nakedGraphicPath !=null)
                        {
                            if ((ContentFinder<Texture2D>.Get((nakedGraphicPath + breastSeverityStringCache + "_south"), false) != null))
                            {
                                if (SizedApparelSettings.matchBodyTextureToMinimumApparelSize ? SizedApparelUtility.BreastSizeIndexToSeverity(currentBreastSizeIndex) <= breastSeverityCapToDraw : true)
                                {
                                    nakedGraphic = GraphicDatabase.Get<Graphic_Multi>(nakedGraphicPath + breastSeverityStringCache, pawnRenderer.nakedGraphic.Shader, pawnRenderer.nakedGraphic.drawSize, pawnRenderer.nakedGraphic.color, pawnRenderer.nakedGraphic.colorTwo, pawnRenderer.nakedGraphic.data);
                                    //newAgr.Add(new ApparelGraphicRecord(graphic, agr.sourceApparel));
                                    //validTexture = true;
                                    //Log.Message(path + BreastSeverityString(BreastSeverity, offset, !findBigger) + ":Texture Found");
                                    pawnRenderer.nakedGraphic = nakedGraphic;
                                    validNakedTexture = true;
                                }
                            }
                        }
                        if (validRottingTexture == false && rottingGraphicPath != null)
                        {
                            if ((ContentFinder<Texture2D>.Get((rottingGraphicPath + breastSeverityStringCache + "_south"), false) != null))
                            {
                                if (SizedApparelSettings.matchBodyTextureToMinimumApparelSize ? SizedApparelUtility.BreastSizeIndexToSeverity(currentBreastSizeIndex) <= breastSeverityCapToDraw : true)
                                {
                                    rottingGraphic = GraphicDatabase.Get<Graphic_Multi>(rottingGraphicPath + breastSeverityStringCache, pawnRenderer.rottingGraphic.Shader, pawnRenderer.rottingGraphic.drawSize, pawnRenderer.rottingGraphic.color, pawnRenderer.rottingGraphic.colorTwo, pawnRenderer.rottingGraphic.data);
                                    //newAgr.Add(new ApparelGraphicRecord(graphic, agr.sourceApparel));
                                    //validTexture = true;
                                    //Log.Message(path + BreastSeverityString(BreastSeverity, offset, !findBigger) + ":Texture Found");
                                    pawnRenderer.rottingGraphic = rottingGraphic;
                                    validRottingTexture = true;
                                }
                            }
                        }

                        //search smaller

                        breastSeverityStringCache = SizedApparelUtility.BreastSeverityString(breastSeverity, offset, false, ref currentBreastSizeIndex, ref currentBreastSeverity);

                        if (validNakedTexture == false)
                        {
                            if ((ContentFinder<Texture2D>.Get((nakedGraphicPath + breastSeverityStringCache + "_south"), false) != null))
                            {
                                if (SizedApparelSettings.matchBodyTextureToMinimumApparelSize ? SizedApparelUtility.BreastSizeIndexToSeverity(currentBreastSizeIndex) <= breastSeverityCapToDraw : true)
                                {
                                    nakedGraphic = GraphicDatabase.Get<Graphic_Multi>(nakedGraphicPath + breastSeverityStringCache, pawnRenderer.nakedGraphic.Shader, pawnRenderer.nakedGraphic.drawSize, pawnRenderer.nakedGraphic.color, pawnRenderer.nakedGraphic.colorTwo, pawnRenderer.nakedGraphic.data);
                                    //newAgr.Add(new ApparelGraphicRecord(graphic, agr.sourceApparel));
                                    //validTexture = true;
                                    //Log.Message(path + BreastSeverityString(BreastSeverity, offset, !findBigger) + ":Texture Found");
                                    pawnRenderer.nakedGraphic = nakedGraphic;
                                    validNakedTexture = true;
                                }
                            }
                        }
                        if (validRottingTexture == false)
                        {
                            if ((ContentFinder<Texture2D>.Get((rottingGraphicPath + breastSeverityStringCache + "_south"), false) != null))
                            {
                                if (SizedApparelSettings.matchBodyTextureToMinimumApparelSize ? SizedApparelUtility.BreastSizeIndexToSeverity(currentBreastSizeIndex) <= breastSeverityCapToDraw : true)
                                {
                                    rottingGraphic = GraphicDatabase.Get<Graphic_Multi>(rottingGraphicPath + breastSeverityStringCache, pawnRenderer.rottingGraphic.Shader, pawnRenderer.rottingGraphic.drawSize, pawnRenderer.rottingGraphic.color, pawnRenderer.rottingGraphic.colorTwo, pawnRenderer.rottingGraphic.data);
                                    //newAgr.Add(new ApparelGraphicRecord(graphic, agr.sourceApparel));
                                    //validTexture = true;
                                    //Log.Message(path + BreastSeverityString(BreastSeverity, offset, !findBigger) + ":Texture Found");
                                    pawnRenderer.rottingGraphic = rottingGraphic;
                                    validRottingTexture = true;
                                }
                            }
                        }

                        if (validNakedTexture == true && validRottingTexture == true)
                        {
                            if (SizedApparelSettings.Debug)
                                Log.Message("[Sized Apparel] " + pawn.Name + "'s body texture has changed.");
                            break;
                        }

                        offset++;
                    }
                    */
                }

                //graphicBreasts = SizedApparelUtility.GetBodyPartGraphic(pawn, breastHediff, true, "Breasts", "Breasts");
                //if(graphicBreasts == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                //    graphicBreasts = SizedApparelUtility.GetBodyPartGraphic(pawn, breastHediff, true, "Breasts", "Breasts", false, true, "Humanlike");
                string bodyPartPath = null;

                SizedApparelsDatabase.PathAndSize cachedPathAndSize;

                float breastSeverityCapped = breastSeverity;
                if (SizedApparelSettings.useBreastSizeCapForApparels) //SizedApparelSettings.useBreastSizeCapForApparels //wip
                    breastSeverityCapped = Math.Min(BreastSeverityCache, breastSeverityCapped);

                string bodyTypeDefName = null;
                if (pawn.story != null)
                    bodyTypeDefName = pawn.story.bodyType?.defName;

                if(breastHediff != null)
                {
                    bodyPartBreasts.SetHediffData(breastHediff.def.defName, SizedApparelUtility.BreastSeverityInt(breastHediff.Severity), SizedApparelUtility.BreastSeverityInt(breastSeverityCapped));
                    bodyPartBreasts.UpdateGraphic();

                    /*
                    cachedPathAndSize = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, bodyTypeDefName, breastHediff.def.defName, "Breasts", SizedApparelUtility.BreastSeverityInt(breastSeverityCapped), false, customPose), true, "Breasts", "Breasts");
                    bodyPartPath = cachedPathAndSize.pathWithSizeIndex;
                    if (bodyPartPath != null)
                    {
                        if (cachedPathAndSize.isUnsupportedHumanlikePath)
                        {

                            if (SizedApparelSettings.UnsupportedRaceToUseHumanlike && pawn.RaceProps.Humanlike)
                                graphicBreasts = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                        }
                        else
                            graphicBreasts = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                    }

                    //if (graphicbreasts == null && pawn.raceprops.humanlike && sizedapparelsettings.unsupportedracetousehumanlike)
                    //{
                    //    bodypartpath = sizedapparelsdatabase.getsupportedbodypartpath(new sizedapparelsdatabase.bodypartdatabasekey(pawn.def.defname, pawn.story?.bodytype.defname, breasthediff.def.defname, "breasts", sizedapparelutility.breastseverityint(breastseveritycapped), false), true, "breasts", "breasts", "humanlike").pathwithsizeindex;
                    //    if (bodypartpath != null)
                    //        graphicbreasts = graphicdatabase.get<graphic_multi>(bodypartpath);
                    //}
                    bodyPartPath = null;
                    cachedPathAndSize = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, bodyTypeDefName, breastHediff.def.defName, "Breasts", SizedApparelUtility.BreastSeverityInt(breastSeverityCapped), true, customPose), true, "Breasts", "Breasts");
                    bodyPartPath = cachedPathAndSize.pathWithSizeIndex;
                    if (bodyPartPath != null)
                    {
                        if (cachedPathAndSize.isUnsupportedHumanlikePath)
                        {
                            Shader shader = pawnRenderer.nakedGraphic.Shader;
                            if (shader == ShaderDatabase.CutoutSkin || ShaderDatabase.CutoutSkinColorOverride || ShaderDatabase.Cutout)
                                shader = ShaderDatabase.CutoutComplex;
                            if (SizedApparelSettings.UnsupportedRaceToUseHumanlike && pawn.RaceProps.Humanlike)
                                graphicBreasts_horny = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath,shader);
                        }
                        else
                            graphicBreasts_horny = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                    }*/
                    //if (graphicBreasts_horny == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                    //{
                    //    bodyPartPath = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, pawn.story?.bodyType.defName, breastHediff.def.defName, "Breasts", SizedApparelUtility.BreastSeverityInt(breastSeverityCapped), true), true, "Breasts", "Breasts", "Humanlike").pathWithSizeIndex;
                    //    if (bodyPartPath != null)
                    //        graphicBreasts_horny = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                    //}

                    //graphicBreasts_horny = SizedApparelUtility.GetBodyPartGraphic(pawn, breastHediff, true, "Breasts", "Breasts" , true);
                    //if(graphicBreasts_horny == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                    //    graphicBreasts_horny = SizedApparelUtility.GetBodyPartGraphic(pawn, breastHediff, true, "Breasts", "Breasts", true, true, "Humanlike");

                    /*
                    graphicBreastsNude = SizedApparelUtility.GetBodyPartGraphic(pawn, breastHediff, true, "Breasts", "Breasts");
                    if (graphicBreastsNude == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                        graphicBreastsNude = SizedApparelUtility.GetBodyPartGraphic(pawn, breastHediff, true, "Breasts", "Breasts", false, true, "Humanlike");

                    graphicBreastsNude_horny = SizedApparelUtility.GetBodyPartGraphic(pawn, breastHediff, true, "Breasts", "Breasts", true);
                    if (graphicBreastsNude_horny == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                        graphicBreastsNude_horny = SizedApparelUtility.GetBodyPartGraphic(pawn, breastHediff, true, "Breasts", "Breasts", true, true, "Humanlike");
                    */
                }





                if (udderHediff != null)
                {
                    bodyPartUdder.SetHediffData(udderHediff.def.defName, SizedApparelUtility.BreastSeverityInt(udderHediff.Severity));
                    bodyPartUdder.UpdateGraphic();
                    /*
                    bodyPartPath = null;
                    cachedPathAndSize = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, bodyTypeDefName, udderHediff.def.defName, "Udder", SizedApparelUtility.PrivatePartSeverityInt(udderHediff.Severity), false, customPose), false, "Udder", "Udder");
                    bodyPartPath = cachedPathAndSize.pathWithSizeIndex;
                    if (bodyPartPath != null)
                    {
                        if (cachedPathAndSize.isUnsupportedHumanlikePath)
                        {
                            if (SizedApparelSettings.UnsupportedRaceToUseHumanlike && pawn.RaceProps.Humanlike)
                                graphicUdder = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                        }
                        else
                            graphicUdder = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                    }
                    bodyPartPath = null;
                    cachedPathAndSize = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, bodyTypeDefName, udderHediff.def.defName, "Udder", SizedApparelUtility.PrivatePartSeverityInt(udderHediff.Severity), true), false, "Udder", "Udder");
                    bodyPartPath = cachedPathAndSize.pathWithSizeIndex;
                    if (bodyPartPath != null)
                    {
                        if (cachedPathAndSize.isUnsupportedHumanlikePath)
                        {
                            if (SizedApparelSettings.UnsupportedRaceToUseHumanlike && pawn.RaceProps.Humanlike)
                                graphicUdder_horny = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                        }
                        else
                            graphicUdder_horny = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                    }*/
                }

            
                float bellySeverity = 0;
                List<Hediff> pregnancies = pawn.health?.hediffSet?.hediffs?.FindAll((Hediff h) => h.def.defName.ToLower().Contains("pregnancy"));
                if (!pregnancies.NullOrEmpty())
                {
                    foreach (Hediff h in pregnancies)
                    {
                        bellySeverity = Math.Max(bellySeverity, h.Severity);
                    }
                }
                /*
                List<Hediff> eggs = pawn.health?.hediffSet?.hediffs?.FindAll((Hediff h) => h.def.defName.ToLower().Contains("rjw") && h.def.defName.ToLower().Contains("egg"));
                if (!eggs.NullOrEmpty())
                {
                    foreach (Hediff h in eggs)
                    {
                        bellySeverity = Math.Max(bellySeverity, h.Severity);
                    }
                }
                List<Hediff> mech = pawn.health?.hediffSet?.hediffs?.FindAll((Hediff h) => h.def.defName.ToLower().Contains("rjw") && h.def.defName.ToLower().Contains("mech"));
                if (!mech.NullOrEmpty())
                {
                    foreach (Hediff h in mech)
                    {
                        bellySeverity = Math.Max(bellySeverity, h.Severity);
                    }
                }
                List<Hediff> parasite = pawn.health?.hediffSet?.hediffs?.FindAll((Hediff h) => h.def.defName.ToLower().Contains("parasite"));
                if (!parasite.NullOrEmpty())
                {
                    foreach (Hediff h in parasite)
                    {
                        bellySeverity = Math.Max(bellySeverity, h.Severity);
                    }
                }*/

                //Licentia Lab Hediff
                if (SizedApparelPatch.LicentiaActive)
                {
                    Hediff cumflation = pawn.health?.hediffSet?.GetFirstHediffOfDef(HediffDef.Named("Cumflation"));
                    Hediff cumstuffed = pawn.health?.hediffSet?.GetFirstHediffOfDef(HediffDef.Named("Cumstuffed"));
                    bellySeverity += cumflation != null ? cumflation.Severity : 0;
                    bellySeverity += cumstuffed != null ? cumstuffed.Severity : 0;
                }

                if (bellySeverity > 0)
                {
                    bodyPartBelly.SetHediffData("BellyBulge", SizedApparelUtility.PrivatePartSeverityInt(bellySeverity));
                    bodyPartBelly.UpdateGraphic();
                    /*
                    bodyPartPath = null;
                    cachedPathAndSize = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, bodyTypeDefName, "BellyBulge", "Belly", SizedApparelUtility.PrivatePartSeverityInt(bellySeverity), false), false, "Belly", "BellyBulge");
                    bodyPartPath = cachedPathAndSize.pathWithSizeIndex;
                    if (bodyPartPath != null)
                    {
                        if (cachedPathAndSize.isUnsupportedHumanlikePath)
                        {
                            if (SizedApparelSettings.UnsupportedRaceToUseHumanlike && pawn.RaceProps.Humanlike)
                                graphicBelly = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                        }
                        else
                            graphicBelly = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                    }*/
                    //if (graphicVagina_horny == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                    //{
                    //    bodyPartPath = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, pawn.story?.bodyType.defName, vaginaHediff.def.defName, "Vagina", SizedApparelUtility.PrivatePartSeverityInt(vaginaHediff.Severity), true), false, "Vagina", "Vagina", "Humanlike").pathWithSizeIndex;
                    //    if (bodyPartPath != null)
                    //        graphicVagina_horny = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                    //}
                }



                //graphicVagina = SizedApparelUtility.GetBodyPartGraphic(pawn, vaginaHediff, false, "Vagina", "Vagina");
                //if(graphicVagina == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                //    graphicVagina = SizedApparelUtility.GetBodyPartGraphic(pawn, vaginaHediff, false, "Vagina", "Vagina", false, true,"Humanlike");
                if (vaginaHediff != null)
                {
                    bodyPartVagina.SetHediffData(vaginaHediff.def.defName, SizedApparelUtility.PrivatePartSeverityInt(vaginaHediff.Severity));
                    bodyPartVagina.UpdateGraphic();
                    /*
                    bodyPartPath = null;
                    cachedPathAndSize = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, bodyTypeDefName, vaginaHediff.def.defName, "Vagina", SizedApparelUtility.PrivatePartSeverityInt(vaginaHediff.Severity), false), false, "Vagina", "Vagina");
                    bodyPartPath = cachedPathAndSize.pathWithSizeIndex;
                    if (bodyPartPath != null)
                    {
                        if (cachedPathAndSize.isUnsupportedHumanlikePath)
                        {
                            if (SizedApparelSettings.UnsupportedRaceToUseHumanlike && pawn.RaceProps.Humanlike)
                                graphicVagina = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                        }
                        else
                            graphicVagina = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                    }
                    bodyPartPath = null;
                    cachedPathAndSize = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, bodyTypeDefName, vaginaHediff.def.defName, "Vagina", SizedApparelUtility.PrivatePartSeverityInt(vaginaHediff.Severity), true), false, "Vagina", "Vagina");
                    bodyPartPath = cachedPathAndSize.pathWithSizeIndex;
                    if (bodyPartPath != null)
                    {
                        if (cachedPathAndSize.isUnsupportedHumanlikePath)
                        {
                            if (SizedApparelSettings.UnsupportedRaceToUseHumanlike && pawn.RaceProps.Humanlike)
                                graphicVagina_horny = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                        }
                        else
                            graphicVagina_horny = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                    }*/
                    //if (graphicVagina_horny == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                    //{
                    //    bodyPartPath = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, pawn.story?.bodyType.defName, vaginaHediff.def.defName, "Vagina", SizedApparelUtility.PrivatePartSeverityInt(vaginaHediff.Severity), true), false, "Vagina", "Vagina", "Humanlike").pathWithSizeIndex;
                    //    if (bodyPartPath != null)
                    //        graphicVagina_horny = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                    //}
                }

                //graphicVagina_horny = SizedApparelUtility.GetBodyPartGraphic(pawn, vaginaHediff, false, "Vagina", "Vagina", true);
                //if(graphicVagina_horny == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                //    graphicVagina_horny = SizedApparelUtility.GetBodyPartGraphic(pawn, vaginaHediff, false, "Vagina", "Vagina", true, true, "Humanlike");
                /*
                graphicPenises.Clear();
                graphicPenises_horny.Clear();
                graphicBallsOfPenises.Clear();
                */

                //TODO: Optimize
                bodyPartPenises.Clear();
                bodyPartBalls.Clear();
                if (penisHediffs != null)
                {


                    for (int i = 0; i < penisHediffs.Count; i++)
                    {
                        Hediff penisHediff = penisHediffs[i];
                        float offsetX = 0; // right and left
                        float offsetZ = 0; // up and down
                        float t = (i + 1f) / (penisHediffs.Count + 1);
                        offsetX = Mathf.Lerp(-0.05f, 0.05f, t);
                        offsetZ = Mathf.Lerp(-0.02f, 0.02f, t);

                        if (penisHediff == null)
                            continue;



                        SizedApparelBodyPart penis = new SizedApparelBodyPart(pawn, "Penis", SizedApparelBodyPartOf.Penis, "Penis", false, false);
                        penis.SetDepthOffsets(0.0108f, 0.0025f, 0.0108f, 0.0108f);
                        penis.SetPositionOffsets(new Vector2(offsetX, 0), new Vector2(-offsetX, 0), new Vector2(offsetX, offsetZ), new Vector2(offsetX, -offsetZ));
                        penis.SetHediffData(penisHediff.def.defName, SizedApparelUtility.PrivatePartSeverityInt(penisHediff.Severity));
                        penis.UpdateGraphic();
                        bodyPartPenises.Add(penis);

                        SizedApparelBodyPart balls = new SizedApparelBodyPart(pawn, "Balls", SizedApparelBodyPartOf.Penis, "Penis", false, false, "Penis/Balls");
                        balls.SetDepthOffsets(0.0096f, 0.0085f, 0.0096f, 0.0096f);
                        balls.SetPositionOffsets(new Vector2(offsetX, 0), new Vector2(-offsetX, 0), new Vector2(offsetX, offsetZ), new Vector2(offsetX, -offsetZ));
                        balls.SetHediffData(penisHediff.def.defName, SizedApparelUtility.PrivatePartSeverityInt(penisHediff.Severity));
                        balls.UpdateGraphic();
                        bodyPartBalls.Add(balls);


                        /*
                        Graphic penisGraphic = SizedApparelUtility.GetBodyPartGraphic(pawn, penisHediff, false, "Penis", "Penis");
                        if (penisGraphic == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                            penisGraphic = SizedApparelUtility.GetBodyPartGraphic(pawn, penisHediff, false, "Penis", "Penis", false, true, "Humanlike");

                        Graphic penisGraphicHorny = SizedApparelUtility.GetBodyPartGraphic(pawn, penisHediff, false, "Penis", "Penis", true);
                        if (penisGraphicHorny == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                            penisGraphicHorny = SizedApparelUtility.GetBodyPartGraphic(pawn, penisHediff, false, "Penis", "Penis", true, true, "Humanlike");
                        */
                        /*
                        Graphic penisGraphic = null;
                        bodyPartPath = null;
                        cachedPathAndSize = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, bodyTypeDefName, penisHediff.def.defName, "Penis", SizedApparelUtility.PrivatePartSeverityInt(penisHediff.Severity), false), false, "Penis", "Penis");
                        bodyPartPath = cachedPathAndSize.pathWithSizeIndex;
                        if (bodyPartPath != null)
                        {
                            if (cachedPathAndSize.isUnsupportedHumanlikePath)
                            {
                                if (SizedApparelSettings.UnsupportedRaceToUseHumanlike && pawn.RaceProps.Humanlike)
                                    penisGraphic = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                            }
                            else
                                penisGraphic = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                        }
                        //if (penisGraphic == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                        //{
                        //    bodyPartPath = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, pawn.story?.bodyType.defName, penisHediff.def.defName, "Penis", SizedApparelUtility.PrivatePartSeverityInt(penisHediff.Severity), false), false, "Penis", "Penis", "Humanlike").pathWithSizeIndex;
                        //    if (bodyPartPath != null)
                        //        penisGraphic = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                        //}
                        Graphic penisGraphicHorny = null;
                        bodyPartPath = null;
                        cachedPathAndSize = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, bodyTypeDefName, penisHediff.def.defName, "Penis", SizedApparelUtility.PrivatePartSeverityInt(penisHediff.Severity), true), false, "Penis", "Penis");
                        bodyPartPath = cachedPathAndSize.pathWithSizeIndex;
                        if (bodyPartPath != null)
                        {
                            if (cachedPathAndSize.isUnsupportedHumanlikePath)
                            {
                                if (SizedApparelSettings.UnsupportedRaceToUseHumanlike && pawn.RaceProps.Humanlike)
                                    penisGraphicHorny = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                            }
                            else
                                penisGraphicHorny = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                        }
                        //if (penisGraphicHorny == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                        //{
                        //    bodyPartPath = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, pawn.story?.bodyType.defName, penisHediff.def.defName, "Penis", SizedApparelUtility.PrivatePartSeverityInt(penisHediff.Severity), true), false, "Penis", "Penis", "Humanlike").pathWithSizeIndex;
                        //    if (bodyPartPath != null)
                        //        penisGraphicHorny = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                        //}

                        //balls are no need horny graphic. it wiil share sametime
                        
                        //Graphic ballGraphic = SizedApparelUtility.GetBodyPartGraphic(pawn, penisHediff, false, "Penis/Balls", "Penis", false);
                        //if (ballGraphic == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                        //    ballGraphic = SizedApparelUtility.GetBodyPartGraphic(pawn, penisHediff, false, "Penis/Balls", "Penis", false, true, "Humanlike");
                        

                        Graphic ballGraphic = null;
                        bodyPartPath = null;
                        cachedPathAndSize = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, bodyTypeDefName, "Penis/Balls", penisHediff.def.defName, SizedApparelUtility.PrivatePartSeverityInt(penisHediff.Severity), false), false, "Penis/Balls", "Penis");
                        bodyPartPath = cachedPathAndSize.pathWithSizeIndex;
                        if (bodyPartPath != null)
                        {
                            if (cachedPathAndSize.isUnsupportedHumanlikePath)
                            {
                                if (SizedApparelSettings.UnsupportedRaceToUseHumanlike && pawn.RaceProps.Humanlike)
                                    ballGraphic = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                            }
                            else
                                ballGraphic = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                        }
                        //if (ballGraphic == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                        //{
                        //    bodyPartPath = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, pawn.story?.bodyType.defName, "Penis/Balls", penisHediff.def.defName, SizedApparelUtility.PrivatePartSeverityInt(penisHediff.Severity), false), false, "Penis/Balls", "Penis", "Humanlike").pathWithSizeIndex;
                        //    if (bodyPartPath != null)
                        //        ballGraphic = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                        //}


                        if (true)//penisGraphic != null
                        {
                            graphicPenises.Add(penisGraphic);
                            graphicPenises_horny.Add(penisGraphicHorny);
                            graphicBallsOfPenises.Add(ballGraphic);
                        }
                        */
                    }
                }
                /*
                graphicAnus = SizedApparelUtility.GetBodyPartGraphic(pawn, anusHediff, false, "Anus", "Anus");
                if (graphicAnus == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                    graphicAnus = SizedApparelUtility.GetBodyPartGraphic(pawn, anusHediff, false, "Anus", "Anus", false, true, "Humanlike");

                graphicAnus_horny = SizedApparelUtility.GetBodyPartGraphic(pawn, anusHediff, false, "Anus", "Anus", true);
                if (graphicAnus_horny == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                    graphicAnus_horny = SizedApparelUtility.GetBodyPartGraphic(pawn, anusHediff, false, "Anus", "Anus", true, true, "Humanlike");
                */
                if(anusHediff != null)
                {
                    bodyPartAnus.SetHediffData(anusHediff.def.defName, SizedApparelUtility.PrivatePartSeverityInt(anusHediff.Severity));
                    bodyPartAnus.UpdateGraphic();

                    /*
                    bodyPartPath = null;
                    cachedPathAndSize = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, bodyTypeDefName, anusHediff.def.defName, "Anus", SizedApparelUtility.PrivatePartSeverityInt(anusHediff.Severity), false), false, "Anus", "Anus");
                    bodyPartPath = cachedPathAndSize.pathWithSizeIndex;
                    if (bodyPartPath != null)
                    {
                        if (cachedPathAndSize.isUnsupportedHumanlikePath)
                        {
                            if (SizedApparelSettings.UnsupportedRaceToUseHumanlike && pawn.RaceProps.Humanlike)
                                graphicAnus = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                        }
                        else
                            graphicAnus = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                    }
                    //if (graphicAnus == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                    //{
                    //    bodyPartPath = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, pawn.story?.bodyType.defName, anusHediff.def.defName, "Anus", SizedApparelUtility.PrivatePartSeverityInt(anusHediff.Severity), false), false, "Anus", "Anus", "Humanlike").pathWithSizeIndex;
                    //    if (bodyPartPath != null)
                    //        graphicAnus = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                    //}
                    bodyPartPath = null;
                    cachedPathAndSize = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, bodyTypeDefName, anusHediff.def.defName, "Anus", SizedApparelUtility.PrivatePartSeverityInt(anusHediff.Severity), true), false, "Anus", "Anus");
                    bodyPartPath = cachedPathAndSize.pathWithSizeIndex;
                    if (bodyPartPath != null)
                    {
                        if (cachedPathAndSize.isUnsupportedHumanlikePath)
                        {
                            if (SizedApparelSettings.UnsupportedRaceToUseHumanlike && pawn.RaceProps.Humanlike)
                                graphicAnus_horny = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                        }
                        else
                            graphicAnus_horny = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                    }
                    //if (graphicAnus_horny == null && pawn.RaceProps.Humanlike && SizedApparelSettings.UnsupportedRaceToUseHumanlike)
                    //{
                    //    bodyPartPath = SizedApparelsDatabase.GetSupportedBodyPartPath(new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, pawn.story?.bodyType.defName, anusHediff.def.defName, "Anus", SizedApparelUtility.PrivatePartSeverityInt(anusHediff.Severity), true), false, "Anus", "Anus", "Humanlike").pathWithSizeIndex;
                    //    if (bodyPartPath != null)
                    //        graphicAnus_horny = GraphicDatabase.Get<Graphic_Multi>(bodyPartPath);
                    //}*/
                }



                hasGraphicUpdatedBefore = true;
            }

            if(CheckApparel)
                cachedApparelGraphicRecord = pawnRenderer.apparelGraphics.ToList();
            else
                cachedApparelGraphicRecord = new List<ApparelGraphicRecord>();

            hasUpdateBeforeSuccess = true;
        }





        //public bool hasUnsupportedApparel => Props.hasUnsupportedApparel;
        //public bool hasUpdateBefore => Props.hasUpdateBefore;
        /*
        public void SetHasUnsupportedApparel(bool _hasUnsupportedApparel)
        {
            if(Props !=null)
            Props.hasUnsupportedApparel = _hasUnsupportedApparel;
            this.hasUnsupportedApparel = _hasUnsupportedApparel;
        }
        public void SetHasUpdateBefore(bool _hasUpdateBefore)
        {
            if (Props != null)
                Props.hasUpdateBefore = _hasUpdateBefore;
            this.hasUpdateBefore = _hasUpdateBefore;
        }*/
        public IEnumerable<SizedApparelBodyPart> GetAllSizedApparelBodyPart() // can return null bodyparts
        {
            yield return bodyPartBreasts;
            yield return bodyPartNipple; //TODO
            foreach(SizedApparelBodyPart bp in bodyPartPenises)
            {
                yield return bp;
            }
            foreach (SizedApparelBodyPart bp in bodyPartBalls)
            {
                yield return bp;
            }
            yield return bodyPartVagina;
            yield return bodyPartAnus;
            yield return bodyPartBelly;
            yield return bodyPartMuscleOverlay;//TODO
            yield return bodyPartUdder;
            yield return bodyPartPubicHair; //TODO
            yield return bodyPartHips;
            foreach (SizedApparelBodyPart bp in bodyPartThighs)
            {
                yield return bp;
            }
            foreach (SizedApparelBodyPart bp in bodyPartHands)
            {
                yield return bp;
            }
            foreach (SizedApparelBodyPart bp in bodyPartFeet)
            {
                yield return bp;
            }
        }
        public IEnumerable<SizedApparelBodyPart> GetSizedApparelBodyParts(SizedApparelBodyPartOf targetPartOf, bool returnNullPart = false)
        {
            foreach(SizedApparelBodyPart bp in GetAllSizedApparelBodyPart())
            {
                if (bp == null)
                {
                    if (returnNullPart)
                        yield return bp;
                    else
                        continue;
                }

                
                if (bp.bodyPartOf.IsPartOf(targetPartOf))
                    yield return bp;
            }
        }

        public void SetPoseFromPoseSet(string poseSetName, bool autoUpdate = true, bool autoSetPawnGraphicDirty = false)
        {
            if (poseSetName == null)
            {
                ClearAllPose(autoUpdate, autoSetPawnGraphicDirty);
                return;
            }

            var poseSetDef = DefDatabase<SizedApparelPoseSetDef>.GetNamed(poseSetName,false);
            if (poseSetDef == null)
                return;
            if (poseSetDef.poses.NullOrEmpty())
                return;
            foreach (SizedApparelPose pose in poseSetDef.poses)
            {
                var bodyParts = GetSizedApparelBodyParts(pose.targetBodyPart);
                //if (bodyParts == null)
                //    continue;
                if (bodyParts.EnumerableNullOrEmpty())
                    continue;
                foreach (SizedApparelBodyPart bp in bodyParts.ToList())
                {
                    if(bp != null)
                        bp.SetCustomPose(poseSetName, autoUpdate, autoSetPawnGraphicDirty);
                }
            }
        }
        public void ClearAllPose(bool autoUpdate = true, bool autoSetPawnGraphicDirty = false)
        {
            foreach (SizedApparelBodyPart bp in GetAllSizedApparelBodyPart())
            {
                if(bp != null)
                    bp.SetCustomPose(null, autoUpdate, autoSetPawnGraphicDirty);
            }
        }
        public void ClearPose(SizedApparelBodyPartOf targetPartOf , bool autoUpdate = true, bool autoSetPawnGraphicDirty = false)
        {
            foreach (SizedApparelBodyPart bp in GetSizedApparelBodyParts(targetPartOf))
            {
                if(bp != null)
                    bp.SetCustomPose(null, autoUpdate, autoSetPawnGraphicDirty);
            }
        }

    }
    [StaticConstructorOnStartup]
    public class ApparelRecorderCompProperties : CompProperties
    {
        public bool hasUnsupportedApparel = false;
        public bool hasUpdateBefore = false;

        public ApparelRecorderCompProperties()
        {
            this.compClass = typeof(ApparelRecorderComp);
        }
        public ApparelRecorderCompProperties(Type compClass) : base(compClass)
        {
            this.compClass = compClass;
        }

    }
}
