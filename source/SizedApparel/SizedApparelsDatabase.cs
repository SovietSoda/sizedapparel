﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RimWorld;
using Verse;
using HarmonyLib;
using UnityEngine;
using rjw;

namespace SizedApparel
{

    public static class SizedApparelsDatabase
    {
        public struct SizedApparelDatabaseKey
        {
            public string pathWithoutSizeIndex; // Do Not Include Size Data to path! bodytype could be included
            public string raceName;
            public string bodyTypeName;
            public string hediffName;
            public int targetSize;
            public bool isHorny;
            public string customPose;


            public SizedApparelDatabaseKey(string path, string race, string bodyType = null, string hediff = null, int size = -1, bool horny = false, string customPose = null)
            {
                this.pathWithoutSizeIndex = path;
                this.raceName = race;
                this.bodyTypeName = bodyType;
                this.hediffName = hediff;
                this.targetSize = size;
                this.isHorny = horny;
                this.customPose = customPose;
            }
        }
        public struct SizedApparelDatabaseKeyComparer : IEqualityComparer<SizedApparelDatabaseKey>
        {
            public bool Equals(SizedApparelDatabaseKey x, SizedApparelDatabaseKey y)
            {
                if (x.targetSize != y.targetSize)
                    return false;
                return true && (x.pathWithoutSizeIndex == y.pathWithoutSizeIndex) && (x.bodyTypeName == y.bodyTypeName) && (x.raceName == y.raceName) && (x.hediffName == y.hediffName) && (x.isHorny == y.isHorny) && (x.customPose == y.customPose);
            }

            public int GetHashCode(SizedApparelDatabaseKey obj)
            {
                return obj.GetHashCode();
            }
        }
        public struct BodyPartDatabaseKey
        {
            public string raceName;
            public string bodyTypeName;
            public string hediffName;
            public string folderPath;
            public int targetSize;
            public bool isHorny;
            public string customPose; // null custom pose as default pose

            public BodyPartDatabaseKey(string race, string bodyType = null, string hediff = null, string path = null, int size = -1, bool horny = false, string customPose = null)
            {
                this.raceName = race;
                this.bodyTypeName = bodyType;
                this.hediffName = hediff;
                this.folderPath = path;
                this.targetSize = size;
                this.isHorny = horny;
                this.customPose = customPose;
            }
        }
        public struct BodyPartDatabaseKeyComparer : IEqualityComparer<BodyPartDatabaseKey>
        {
            public bool Equals(BodyPartDatabaseKey x, BodyPartDatabaseKey y)
            {
                if (x.targetSize != y.targetSize)
                    return false;
                return true && (x.raceName == y.raceName) && (x.bodyTypeName == y.bodyTypeName) && (x.hediffName == y.hediffName) && ( x.folderPath == y.folderPath) && (x.isHorny == y.isHorny) && (x.customPose == y.customPose);
            }

            public int GetHashCode(BodyPartDatabaseKey obj)
            {
                return obj.GetHashCode();
            }
        }
        public struct PathAndSize
        {
            public string pathWithSizeIndex;
            public int size;
            public bool isUnsupportedHumanlikePath;
            public bool isCustomPose;
            public string hediffName;
            public Dictionary<string, BodyPartPoint> points;

            public PathAndSize(string path, int index, bool unsupportedHumanlike = false, bool customPose = false, string hediff = null ,Dictionary<string, BodyPartPoint> pointsInput = null)
            {
                this.pathWithSizeIndex = path;
                this.size = index;
                this.isUnsupportedHumanlikePath = unsupportedHumanlike;
                this.isCustomPose = customPose;
                this.hediffName = hediff;
                this.points = pointsInput;
            }
        }

        private static Dictionary<SizedApparelDatabaseKey, PathAndSize> SupportedApparelResultPath = new Dictionary<SizedApparelDatabaseKey, PathAndSize>(new SizedApparelDatabaseKeyComparer());
        private static Dictionary<BodyPartDatabaseKey, PathAndSize> SupportedBodyPartResultPath = new Dictionary<BodyPartDatabaseKey, PathAndSize>(new BodyPartDatabaseKeyComparer());
        private static Dictionary<string, string> SupportedApparelOriginalPath = new Dictionary<string, string>();

        public static void ClearAll()
        {
            SupportedApparelResultPath.Clear();
            SupportedBodyPartResultPath.Clear();
            SupportedApparelOriginalPath.Clear();
        }

        //Apparels, Bodyparts can be used
        public static PathAndSize GetSupportedApparelSizedPath(SizedApparelDatabaseKey key)
        {
            int currentSize = -1;
            float currentSeverity = -1;
            return GetSupportedApparelSizedPath(key, out currentSize, out currentSeverity);

        }

        public static string GetSupportedApparelOriginalPath(string path)
        {
            string outString;
            if (SupportedApparelOriginalPath.TryGetValue(path, out outString))
                return outString;
            return null;
        }

        public static Dictionary<string, BodyPartPoint> GetGraphicPoints(string textuerPath)
        {
            //DefDatabase<>
            return null;
        }

        public static PathAndSize GetSupportedApparelSizedPath(SizedApparelDatabaseKey key, out int indexOut, out float currentSeverityOut)
        {
            if (SupportedApparelResultPath.ContainsKey(key))
            {
                if (SizedApparelSettings.Debug)
                    Log.Message("[Sized Apparel] SizedApparelDataBase::ValidKey: "+ key.pathWithoutSizeIndex);
                var value =  SupportedApparelResultPath.TryGetValue(key);
                indexOut = value.size;
                currentSeverityOut = SizedApparelUtility.BreastSizeIndexToSeverity(value.size);
                if (SizedApparelSettings.Debug)
                    Log.Message("[Sized Apparel] SizedApparelDataBase::Result Path: " + value.pathWithSizeIndex);
                return value;
            }
            if (SizedApparelSettings.Debug)
                Log.Message("  [Sized Apparel] SizedApparelDataBase::Key Not Found: " + key.pathWithoutSizeIndex);

            PathAndSize result;
            Graphic sourceGraphic = GraphicDatabase.Get<Graphic_Multi>(key.pathWithoutSizeIndex);
            bool flag;
            bool customPose = true; //default none pose also custom pose
            string hediffResult;
            Graphic graphic = null;
            graphic = SizedApparelUtility.GetSizedApparelGraphic(sourceGraphic, SizedApparelUtility.BreastSizeIndexToSeverity(key.targetSize), out indexOut, out currentSeverityOut, out flag, out hediffResult, key.raceName, key.hediffName, key.customPose);//key.customPose
            if (key.customPose != null && graphic == null)
            {
                customPose = false;
                graphic = SizedApparelUtility.GetSizedApparelGraphic(sourceGraphic, SizedApparelUtility.BreastSizeIndexToSeverity(key.targetSize), out indexOut, out currentSeverityOut, out flag, out hediffResult);
            }

            //Try Find Different Target Size
            if (flag == true)
            {
                result = new PathAndSize(graphic.path, indexOut, false, customPose, hediffResult);
                SupportedApparelResultPath.SetOrAdd(key, result);
                SupportedApparelOriginalPath.SetOrAdd(result.pathWithSizeIndex, key.pathWithoutSizeIndex);
            }
            else
            {
                result = new PathAndSize(null, -1);
                SupportedApparelResultPath.SetOrAdd(key, result);
                SupportedApparelOriginalPath.SetOrAdd(key.pathWithoutSizeIndex, key.pathWithoutSizeIndex);
            }
            return result;
        }

        public static PathAndSize GetSupportedBodyPartPath(BodyPartDatabaseKey key, bool isBreast, string folderName, string defaultHediffName, string customRaceDefName = null)
        {
            PathAndSize result;
            if (SupportedBodyPartResultPath.ContainsKey(key))
                return SupportedBodyPartResultPath.TryGetValue(key);
            int currentSize = -1;
            string hediffResult;
            Graphic graphic = null;
            if (key.customPose != null)
            {
                graphic = SizedApparelUtility.GetBodyPartGraphic(key.raceName, key.bodyTypeName, key.hediffName, isBreast, key.targetSize, folderName+"/CustomPose/"+key.customPose, defaultHediffName, out currentSize, out hediffResult, key.isHorny, customRaceDefName);
                if (graphic != null)
                {
                    result = new PathAndSize(graphic.path, currentSize, false, true, hediffResult);
                    SupportedBodyPartResultPath.SetOrAdd(key, result);
                    return result;
                }
                if (key.bodyTypeName != null)
                    graphic = SizedApparelUtility.GetBodyPartGraphic(key.raceName, null, key.hediffName, isBreast, key.targetSize, folderName + "/CustomPose/" + key.customPose, defaultHediffName, out currentSize, out hediffResult, key.isHorny, customRaceDefName);
                if (graphic != null)
                {
                    result = new PathAndSize(graphic.path, currentSize, false, true, hediffResult);
                    SupportedBodyPartResultPath.SetOrAdd(key, result);
                    return result;
                }

            }
            graphic = SizedApparelUtility.GetBodyPartGraphic(key.raceName, key.bodyTypeName, key.hediffName, isBreast, key.targetSize, folderName, defaultHediffName, out currentSize, out hediffResult, key.isHorny, customRaceDefName);
            if (graphic != null)
            {
                result = new PathAndSize(graphic.path, currentSize, false, key.customPose == null ? true : false, hediffResult);
                SupportedBodyPartResultPath.SetOrAdd(key, result);
                return result;
            }

            if (key.bodyTypeName != null)
                graphic = SizedApparelUtility.GetBodyPartGraphic(key.raceName, null, key.hediffName, isBreast, key.targetSize, folderName, defaultHediffName, out currentSize, out hediffResult, key.isHorny, customRaceDefName);
            if (graphic != null)
            {
                result = new PathAndSize(graphic.path, currentSize, false, key.customPose == null ? true : false, hediffResult);
                SupportedBodyPartResultPath.SetOrAdd(key, result);
                return result;
            }

            //HumanLike Search

            if (key.customPose != null)
            {
                graphic = SizedApparelUtility.GetBodyPartGraphic("Humanlike", key.bodyTypeName, key.hediffName, isBreast, key.targetSize, folderName + "/CustomPose/" + key.customPose, defaultHediffName, out currentSize, out hediffResult, key.isHorny, customRaceDefName);
                if (graphic != null)
                {
                    result = new PathAndSize(graphic.path, currentSize, true, true, hediffResult);
                    SupportedBodyPartResultPath.SetOrAdd(key, result);
                    return result;
                }
                if (key.bodyTypeName != null)
                    graphic = SizedApparelUtility.GetBodyPartGraphic("Humanlike", null, key.hediffName, isBreast, key.targetSize, folderName + "/CustomPose/" + key.customPose, defaultHediffName, out currentSize, out hediffResult, key.isHorny, customRaceDefName);
                if (graphic != null)
                {
                    result = new PathAndSize(graphic.path, currentSize, true, true, hediffResult);
                    SupportedBodyPartResultPath.SetOrAdd(key, result);
                    return result;
                }

            }
            graphic = SizedApparelUtility.GetBodyPartGraphic("Humanlike", key.bodyTypeName, key.hediffName, isBreast, key.targetSize, folderName, defaultHediffName, out currentSize, out hediffResult, key.isHorny, customRaceDefName);
            if (graphic != null)
            {
                result = new PathAndSize(graphic.path, currentSize, true, key.customPose == null ? true : false, hediffResult);
                SupportedBodyPartResultPath.SetOrAdd(key, result);
                return result;
            }

            if (key.bodyTypeName != null)
                graphic = SizedApparelUtility.GetBodyPartGraphic("Humanlike", null, key.hediffName, isBreast, key.targetSize, folderName, defaultHediffName, out currentSize, out hediffResult, key.isHorny, customRaceDefName);
            if (graphic != null)
            {
                result = new PathAndSize(graphic.path, currentSize, true, key.customPose == null ? true : false, hediffResult);
                SupportedBodyPartResultPath.SetOrAdd(key, result);
                return result;
            }



            //Cannot find Any result
            result = new PathAndSize(null, -1);
            SupportedBodyPartResultPath.SetOrAdd(key, result);
            return result;
            
            return result;
        }

    }
}
