﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RimWorld;
using Verse;
using HarmonyLib;
using AlienRace;
using UnityEngine;
using rjw;
using System.Reflection;

namespace SizedApparel
{
    public struct Rot4Offsets
    {
        //X: right and left
        //Y: Frong or Back
        //Z: Up and Down
        Vector3? South;
        Vector3? North;
        Vector3? East;
        Vector3? West;
    }

    public struct RaceNameAndBodyType
    {
        public string raceName;
        public string bodyType;
    }

    public struct BodyTypeAndOffset
    {
        public RaceNameAndBodyType bodyTypeData;
        public Rot4Offsets? Offsets;
    }

    public enum SizedApparelBodyPartColorOf
    {
        Skin, Hair, Custom, None
    }

    [Obsolete]
    public struct SizedApparelBodyPartColor
    {
        public SizedApparelBodyPartColorOf colorMode;
        public Color? customColor;
    }


    public enum SizedApparelBodyPartOf
    {
        All, Torso, Breasts, Crotch, Penis, Vagina, Anus, Belly, Udder, Hips, Thighs, hands, feet, None
    }
    public static class SizedApparelBodyPartOfExtension
    {
        public static bool IsPartOf(this SizedApparelBodyPartOf source, SizedApparelBodyPartOf target)
        {
            if (source == SizedApparelBodyPartOf.None)
                return false;

            switch (target)
            {
                case SizedApparelBodyPartOf.All:
                    return true;
                case SizedApparelBodyPartOf.Torso:
                    if (source == SizedApparelBodyPartOf.hands || source == SizedApparelBodyPartOf.feet)
                        return false;
                    return true;
                case SizedApparelBodyPartOf.Breasts:
                    if (source == SizedApparelBodyPartOf.Breasts)
                        return true;
                    return false;
                case SizedApparelBodyPartOf.Crotch:
                    if (source == SizedApparelBodyPartOf.Penis || source == SizedApparelBodyPartOf.Vagina || source == SizedApparelBodyPartOf.Anus)
                        return true;
                    return false;
                case SizedApparelBodyPartOf.Penis:
                    if (source == SizedApparelBodyPartOf.Penis)
                        return true;
                    return false;
                case SizedApparelBodyPartOf.Vagina:
                    if (source == SizedApparelBodyPartOf.Vagina)
                        return true;
                    return false;
                case SizedApparelBodyPartOf.Anus:
                    if (source == SizedApparelBodyPartOf.Anus)
                        return true;
                    return false;
                case SizedApparelBodyPartOf.Belly:
                    if (source == SizedApparelBodyPartOf.Belly)
                        return true;
                    return false;
                case SizedApparelBodyPartOf.Udder:
                    if (source == SizedApparelBodyPartOf.Udder)
                        return true;
                    return false;
                case SizedApparelBodyPartOf.Hips:
                    if (source == SizedApparelBodyPartOf.Hips || source == SizedApparelBodyPartOf.Thighs || source == SizedApparelBodyPartOf.Penis || source == SizedApparelBodyPartOf.Vagina || source == SizedApparelBodyPartOf.Anus)
                        return true;
                    return false;
                case SizedApparelBodyPartOf.Thighs:
                    if (source == SizedApparelBodyPartOf.Thighs)
                        return true;
                    return false;
                case SizedApparelBodyPartOf.hands:
                    if (source == SizedApparelBodyPartOf.hands)
                        return true;
                    return false;
                case SizedApparelBodyPartOf.feet:
                    if (source == SizedApparelBodyPartOf.feet)
                        return true;
                    return false;
                case SizedApparelBodyPartOf.None:
                    return false;

                    return false; 
            }
            Log.Error("[SizedApparel] missing SizedApparelBodyPartOf!");
            return false;
        }
    }


    public class GraphicPointsDef : Def
    {
        public List<TextureWithGraphicPoints> points;
    }

    public class TextureWithGraphicPoints
    {
        public string texturePath; // texture is already classified with bodytype 
        public List<GraphicPoint> points = new List<GraphicPoint>();
    }

    public class GraphicPoint
    {
        public string pointName;
        public Vector2 point = new Vector2(0.5f, 0.5f);
    }
    public class GraphicPointsWithBodyType
    {
        public string pointName;
        public List<PointWithBodyType> points = new List<PointWithBodyType>();
    }

    public class PointWithBodyType
    {
        public string bodyTypeName; //null can be used too
        public Vector2 point = new Vector2(0.5f,0.5f);
    }

    public struct BodyPartPoint
    {
        string name;
        Vector2 position;//Uv position. not pixel
    }

    [Obsolete]//todo
    public struct BodyPartSpline
    {

    }
    
    //Def per graphic(texture)
    public class SizedApparelBodyPartGraphicDef : Def
    {
        public string graphicPath;
        public int severityIndex;
        public Vector2 pivot = new Vector2(0.5f, 0.5f); // custom pivot of texture. UV. not pixel
        //public Dictionary<string, BodyPartPoint> points = new Dictionary<string, BodyPartPoint>();
        //public Dictionary<string, BodyPartSpline> splines = new Dictionary<string, BodyPartSpline>();

    }

    //Def per BodyParts
    public class SizedApparelBodyPartDef : Def
    {
        SizedApparelBodyPartOf bodyPartOf = SizedApparelBodyPartOf.None;
        public bool canPose = false;
        public bool canAnimate = false;
    }

    public class SizedApparelBodyPart
    {
        static MethodInfo overrideMatMethod = AccessTools.Method(typeof(PawnRenderer), "OverrideMaterialIfNeeded");

        public SizedApparelBodyPart(Pawn pawn, string bodyPartName, SizedApparelBodyPartOf bodyPartOf, string defaultHediffName, bool isBreast, bool isOverlay , string customPathName = null, SizedApparelBodyPartColorOf colorOf = SizedApparelBodyPartColorOf.Skin)
        {
            this.pawn = pawn;
            this.bodyPartName = bodyPartName;

            this.def = DefDatabase<SizedApparelBodyPartDef>.AllDefs.FirstOrDefault(b => b.defName == bodyPartName);

            this.bodyPartOf = bodyPartOf;
            this.defaultHediffName = defaultHediffName;
            this.isBreast = isBreast;
            this.isOverlay = isOverlay;
            this.customPath = customPathName;
            this.colorType = colorOf;
        }

        public Vector2 OffsetFromUVOffset(Vector2 vector, Mesh mesh , bool isFliped = false)
        {
            //treat mesh as plane
            //Vector3 width = mesh.vertices[2] - mesh.vertices[1];
            //Vector3 height = mesh.vertices[1] - mesh.vertices[2];
            Vector2 loc = new Vector2(0.5f, 0.5f) - vector;
            if(!isFliped)
                return new Vector2(Mathf.Lerp(mesh.vertices[0].x, mesh.vertices[2].x, loc.x), Mathf.Lerp(mesh.vertices[0].z, mesh.vertices[2].z, loc.y));
            return new Vector2(Mathf.Lerp(mesh.vertices[3].x, mesh.vertices[1].x, loc.x), Mathf.Lerp(mesh.vertices[3].z, mesh.vertices[1].z, loc.y));
        }

        //public Vector2 OffestFromUVOffset(Vector2 vector, Vector2 drawSize, bool isFliped = false)

        public SizedApparelBodyPartDef def;

        public Pawn pawn;
        public string bodyPartName; //breast, penis, belly, pubichair... etc. just name. not like architech something
        public string customPath = null;
        public SizedApparelBodyPartOf bodyPartOf = SizedApparelBodyPartOf.None;
        public string defaultHediffName;

        public bool isBreast = false;
        public bool isOverlay = false; //write z cache?

        public string currentHediffName;

        public bool isVisible = true;

        public SizedApparelBodyPartColorOf colorType = SizedApparelBodyPartColorOf.Skin;
        public Color? customColorOne;
        public Color? customColorTwo;


        //customize
        public string customPose = null;
        public Vector2? lookAnLocation = null;
        public Rot4? rotOverride = null;


        public void SetCustomPose(string newPose, bool autoUpdate = true, bool autoSetPawnGraphicDirty = false)
        {
            if (customPose == newPose)
                return;
            customPose = newPose;
            if (autoUpdate)
                this.UpdateGraphic();
            if(autoSetPawnGraphicDirty)
            {
                if (pawn == null)
                    return;
                PortraitsCache.SetDirty(pawn);
                GlobalTextureAtlasManager.TryMarkPawnFrameSetDirty(pawn);
            }
        }

        public bool CheckCanPose(string targetPose, bool checkApparels, bool mustMatchSize)
        {
            if (checkApparels)
            {
                foreach(ApparelGraphicRecord agr in pawn.Drawer.renderer.graphics.apparelGraphics)
                {
                    /*
                    if (!agr.sourceApparel.def.apparel.bodyPartGroups.Any(bpgd => bpgd.defName == "Torso" || bpgd.defName == "Chest"))
                        continue;

                    if (agr.sourceApparel.def.apparel.tags.Any(s => s.ToLower() == "SizedApparel_IgnorePose".ToLower()))
                        continue;
                    */

                    string originalPath = SizedApparelsDatabase.GetSupportedApparelOriginalPath(agr.graphic.path);
                    if (originalPath == null)
                        return false;

                    int outInt = -1;
                    float outFloat = -1;
                    SizedApparelsDatabase.SizedApparelDatabaseKey key = new SizedApparelsDatabase.SizedApparelDatabaseKey(originalPath,pawn.def.defName,pawn.story?.bodyType?.defName, currentHediffName, Math.Min(currentSeverityInt, cappedSeverityInt), false, targetPose);
                    var result =  SizedApparelsDatabase.GetSupportedApparelSizedPath(key, out outInt, out outFloat);
                    if (!result.isCustomPose)
                        return false;
                }
            }
            Graphic graphic = GetBodyPartGraphics(false, mustMatchSize, targetPose);
            Graphic graphicH = GetBodyPartGraphics(true, mustMatchSize, targetPose);
            if (graphic != null || graphicH != null)
                return true;
            return false;
        }


        //TODO...
        public int currentSeverityInt = -1;
        public int cappedSeverityInt = 1000; // supported severity from worn apparel graphics

        public Vector2 pivot = new Vector2(0.5f, 0.5f);

        public Vector2 position = Vector2.zero;//offset from pivot //UV. not pixel

        public Dictionary<string, BodyPartPoint> points;
        public Dictionary<string, BodyPartPoint> pointsHorny;


        public float rotation = 0; // +: rotate right, -: rotate left
        public Vector2 scale = Vector2.one;

        public Graphic bodyPartGraphic;
        public Graphic bodyPartGraphicHorny;

        public Vector2 positionOffset = Vector2.zero; //offset from position //UV. not pixel
        public Vector2 positionOffsetSouth = Vector2.zero;
        public Vector2 positionOffsetNorth = Vector2.zero;
        public Vector2 positionOffsetEast = Vector2.zero;
        public Vector2 positionOffsetWest = Vector2.zero;

        public float depthOffset = 0f;

        public float depthOffsetEast = 0.008f;
        public float depthOffsetWest = 0.008f;
        public float depthOffsetSouth = 0.008f;
        public float depthOffsetNorth = 0.008f;

        public void SetDepthOffsets(float south, float north, float east, float west)
        {
            depthOffsetSouth = south;
            depthOffsetNorth = north;
            depthOffsetEast = east;
            depthOffsetWest = west;
        }
        public void SetPositionOffsets(Vector2 south, Vector2 north, Vector2 east, Vector2 west)
        {
            positionOffsetSouth = south;
            positionOffsetNorth = north;
            positionOffsetEast = east;
            positionOffsetWest = west;
        }
        public Graphic GetBodyPartGraphics(bool isHorny, bool mustMatchSize = false, string poseOverride = null)
        {
            Dictionary<string, BodyPartPoint> var;
            return GetBodyPartGraphics(isHorny, out var, mustMatchSize, poseOverride);
        }

        public Graphic GetBodyPartGraphics(bool isHorny, out Dictionary<string, BodyPartPoint> outPoints, bool mustMatchSize = false ,string poseOverride = null)
        {
            if (pawn == null)
            {
                outPoints = null;
                return null;
            }

            var comp = pawn.TryGetComp<ApparelRecorderComp>();

            if (comp == null)
            {
                outPoints = null;
                return null;
            }

            var key = new SizedApparelsDatabase.BodyPartDatabaseKey(pawn.def.defName, pawn.story?.bodyType?.defName, currentHediffName, customPath==null?bodyPartName: customPath, Math.Min(currentSeverityInt, cappedSeverityInt), isHorny, poseOverride==null?customPose:poseOverride);
            var result = SizedApparelsDatabase.GetSupportedBodyPartPath(key, isBreast, customPath == null ? bodyPartName : customPath, defaultHediffName);

            if (mustMatchSize)
                if (Math.Min(currentSeverityInt, cappedSeverityInt) != result.size)
                {
                    outPoints = null;
                    return null;
                }

            if (result.pathWithSizeIndex == null)
            {
                outPoints = null;
                return null;
            }
            outPoints = result.points;
            return GraphicDatabase.Get<Graphic_Multi>(result.pathWithSizeIndex);
        }

        public void UpdateGraphic()
        {
            bodyPartGraphic = GetBodyPartGraphics(false, out points, false);
            bodyPartGraphicHorny = GetBodyPartGraphics(true, out pointsHorny, false);
        }

        public void UpdateGraphic(int index, int indexCapped = 1000)
        {
            this.currentSeverityInt = index;
            this.cappedSeverityInt = indexCapped;

            UpdateGraphic();
        }

        public void ResetTransform()
        {
            this.position = Vector2.zero;
            this.scale = Vector2.one;
            this.rotation = 0;
        }

        public void ClearGraphics()
        {
            this.bodyPartGraphic = null;
            this.bodyPartGraphicHorny = null;
            this.points = null;
            this.pointsHorny = null;
        }
        public void Clear()
        {
            currentHediffName = null;
            currentSeverityInt = -1;
            cappedSeverityInt = 1000;
            customPose = null;
            rotOverride = null;

            ClearGraphics();
        }
        public void SetHediffData(string name, int severityIndex)
        {
            currentHediffName = name;
            currentSeverityInt = severityIndex;
        }
        public void SetHediffData(string name, int severityIndex, int cappedSeverityIndex)
        {
            currentHediffName = name;
            currentSeverityInt = severityIndex;
            this.cappedSeverityInt = cappedSeverityIndex;
        }

        public void DrawBodyPart (Vector3 rootLoc, float angle, Rot4 facing, RotDrawMode bodyDrawType, PawnRenderFlags flags, Mesh bodyMesh)
        {
            if (!isVisible)
                return;
            if (scale == Vector2.zero)
                return; //Don't draw if scale is zero
            if (pawn == null)
                return;

            PawnRenderer pawnRenderer = pawn.Drawer.renderer;
            Shader shader = ShaderDatabase.CutoutComplex;
            Color drawColor1 = Color.white;
            Color drawColor2 = Color.white;

            bool forceWriteZ = true;

            if(colorType == SizedApparelBodyPartColorOf.Skin)
            {
                forceWriteZ = true;
                if (bodyDrawType == RotDrawMode.Fresh)
                {

                    shader = pawn.Drawer.renderer.graphics.nakedGraphic.Shader;
                    if (!ShaderUtility.SupportsMaskTex(shader))
                        shader = ShaderDatabase.CutoutSkinOverlay;
                    drawColor1 = pawn.Drawer.renderer.graphics.nakedGraphic.Color;
                    drawColor2 = pawn.Drawer.renderer.graphics.nakedGraphic.ColorTwo;
                }
                else if (bodyDrawType == RotDrawMode.Rotting)
                {

                    shader = pawn.Drawer.renderer.graphics.rottingGraphic.Shader;
                    if (!ShaderUtility.SupportsMaskTex(shader))
                        shader = ShaderDatabase.CutoutSkinOverlay;
                    drawColor1 = pawn.Drawer.renderer.graphics.rottingGraphic.Color;
                    drawColor2 = pawn.Drawer.renderer.graphics.nakedGraphic.ColorTwo;
                }
            }
            else if (colorType == SizedApparelBodyPartColorOf.Hair)
            {
                forceWriteZ = false;
                shader = ShaderDatabase.Transparent;
                if(pawn.story != null)
                    drawColor1 = pawn.story.hairColor;
            }
            else if (colorType == SizedApparelBodyPartColorOf.Custom)
            {
                forceWriteZ = true;
                shader = ShaderDatabase.Transparent;
                if(customColorOne != null)
                    drawColor1 = customColorOne.Value;
                if (customColorTwo != null)
                    drawColor2 = customColorTwo.Value;
            }
            else if (colorType == SizedApparelBodyPartColorOf.None)
            {
                forceWriteZ = false;
                shader = ShaderDatabase.Cutout;
            }




            Quaternion quaternion = Quaternion.AngleAxis(angle + rotation, Vector3.up);
            Vector3 vector = rootLoc;

            Rot4 targetRot = facing;
            if (rotOverride != null)
                targetRot = rotOverride.Value;

            if (targetRot == Rot4.South)
            {
                var loc = OffsetFromUVOffset(positionOffsetSouth, bodyMesh);
                vector.x += loc.x;
                vector.z += loc.y;
                vector.y += depthOffsetSouth;
            }
            else if(targetRot == Rot4.North)
            {
                var loc = OffsetFromUVOffset(positionOffsetNorth, bodyMesh);
                vector.x += loc.x;
                vector.z += loc.y;
                vector.y += depthOffsetNorth;
            }
            else if (targetRot == Rot4.East)
            {
                var loc = OffsetFromUVOffset(positionOffsetEast, bodyMesh);
                vector.x += loc.x;
                vector.z += loc.y;
                vector.y += depthOffsetEast;
            }
            else if (targetRot == Rot4.West)
            {
                var loc = OffsetFromUVOffset(positionOffsetWest, bodyMesh);
                vector.x += loc.x;
                vector.z += loc.y;
                vector.y += depthOffsetWest;
            }


            Graphic graphic = null;
            if (SizedApparelUtility.IsHorny(pawn))
                graphic = bodyPartGraphicHorny;
            if (graphic == null)
                graphic = bodyPartGraphic;

            if (graphic == null)
                return;

            Material mat;
            if (!flags.FlagSet(PawnRenderFlags.Cache)&&!isOverlay&& forceWriteZ)
            {
                graphic = graphic.GetColoredVersion(ShaderDatabase.Cutout, drawColor1, drawColor2);
                mat = flags.FlagSet(PawnRenderFlags.Cache) ? graphic.MatAt(targetRot) : (Material)overrideMatMethod.Invoke(pawnRenderer, new object[] { graphic.MatAt(facing), pawn, flags.FlagSet(PawnRenderFlags.Portrait) });
                GenDraw.DrawMeshNowOrLater(bodyMesh, vector, quaternion, mat, flags.FlagSet(PawnRenderFlags.DrawNow)); // draw for writeZ data to solve shadow issue
            }
            graphic = graphic.GetColoredVersion(shader, drawColor1, drawColor2);
            vector.y += 0.00001f;
            mat = flags.FlagSet(PawnRenderFlags.Cache) ? graphic.MatAt(targetRot) : (Material)overrideMatMethod.Invoke(pawnRenderer, new object[] { graphic.MatAt(facing), pawn, flags.FlagSet(PawnRenderFlags.Portrait) });
            GenDraw.DrawMeshNowOrLater(bodyMesh, vector, quaternion, mat, flags.FlagSet(PawnRenderFlags.DrawNow));


        }
    }


    public class SizedApparelBodyDef : Def
    {
        //public List<SizedApparelBodyPartDef> BodyParts;


    }

    public class SizedApparelBody
    {
        public string customPoseOfBody = null;

        public bool canCustomPose()
        {
            //check apparels
            return false;
        }
    }

}
