﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using UnityEngine;
using Verse;

namespace SizedApparel
{
    public class SizedApparelSettings : ModSettings
    {
        public static bool Debug = false;
        public static bool DetailLog = false;

        public static bool useBodyTexture = true;//for user who not use rimnudeworld


        public static bool matchBodyTextureToMinimumApparelSize = true;//for avoiding breasts cliping

        public static bool useBreastSizeCapForApparels = true;

        public static bool DontReplaceBodyTextureOnNude = true;
        //public static bool DontReplaceBodyTextureOnUnsupported = true;

        public static bool UnsupportedRaceToUseHumanlike = true;

            //TODO: Standalone render bodyparts.
        public static bool drawBodyParts = true;//for user who not use rimnudeworld
        public static bool drawBreasts = true;
        public static bool drawPenis = true;
        public static bool drawVagina = true;
        public static bool drawMuscleOverlay = true;
        public static bool drawHips = true;//TODO
        public static bool drawAnus = true;
        public static bool drawBelly = false;//TODO
        public static bool drawUdder = false;//TODO
        public static bool hideBallOfFuta = false;
        public static bool hidePenisOfMale = false;
        public static bool matchBreastToSupportedApparelSize = true;//for avoiding breasts cliping

        public static bool autoJiggleBreasts = true;

        //RimNudeWorld
        public static bool drawSizedApparelBreastsOnlyWorn = true;
        public static bool hideRimNudeWorldBreasts = false;//disable rimnudeworld breasts.

        [Obsolete]
        public static bool useUnsupportedBodyTexture = true;//bodytexture when wearing unsupported apparel.
        public static bool useSafeJobBreasts = true;

        public static bool changeBodyType;
        public static bool fatToFemale;
        public static bool hulkToThin;

        public static bool onlyForFemale = true;
        public static bool useRandomSize = true;//for user who play without rimjobworld
        public static float randomSizeMin = 0.01f;
        public static float randomSizeMax = 1.01f;

        public static bool useTitanic = true;
        public static bool useColossal = true;
        public static bool useGargantuan = true;
        public static bool useMassive = true;
        public static bool useEnormous = true;
        public static bool useHuge = true;
        public static bool useLarge = true;
        public static bool useAverage = true;
        public static bool useSmall = true;
        public static bool useTiny = true;
        public static bool useNipples = true;


        public static bool getUseSettingFromIndex(int target)
        {
            if (target < 0)
                return false;
            else if (target == 0)
                return useNipples;
            else if (target == 1)
                return useTiny;
            else if (target == 2)
                return useSmall;
            else if (target == 3)
                return useAverage;
            else if (target == 4)
                return useLarge;
            else if (target == 5)
                return useHuge;
            else if (target == 6)
                return useEnormous;
            else if (target == 7)
                return useMassive;
            else if (target == 8)
                return useGargantuan;
            else if (target == 9)
                return useColossal;
            else if (target == 10)
                return useTitanic;
            else
                return false;
        }

        
       

        public static bool useUnderBreasts = true;
        public static float UnderBreastsOffset = -0.0013f;

        public override void ExposeData()
        {
            Scribe_Values.Look(ref Debug, "Debug", false);
            Scribe_Values.Look(ref DetailLog, "DetailLog", false);

            Scribe_Values.Look(ref useBreastSizeCapForApparels, "useBreastSizeCapForApparels", true);

            Scribe_Values.Look(ref useBodyTexture, "useBodyTexture", false);
            Scribe_Values.Look(ref matchBodyTextureToMinimumApparelSize, "matchBodyTextureToMinimumApparelSize", true);
            Scribe_Values.Look(ref matchBreastToSupportedApparelSize, "matchBreastToSupportedApparelSize", true);

            Scribe_Values.Look(ref UnsupportedRaceToUseHumanlike, "UnsupportedRaceToUseHumanlike", true);

            Scribe_Values.Look(ref useUnsupportedBodyTexture, "useUnsupportedBodyTexture", true);
            Scribe_Values.Look(ref DontReplaceBodyTextureOnNude, "DontReplaceBodyTextureOnNude", false);

            Scribe_Values.Look(ref hideRimNudeWorldBreasts, "hideRimNudeWorldBreasts", false);
            Scribe_Values.Look(ref useSafeJobBreasts, "useSafeJobBreasts", true);

            Scribe_Values.Look(ref useRandomSize, "useRandomSize", true);
            Scribe_Values.Look(ref randomSizeMin, "randomSizeMin", 0.01f);
            Scribe_Values.Look(ref randomSizeMax, "randomSizeMax", 1.01f);

            Scribe_Values.Look(ref drawBodyParts, "drawBodyParts", true);

            Scribe_Values.Look(ref drawMuscleOverlay, "drawMuscleOverlay", true);
            Scribe_Values.Look(ref drawBreasts, "drawBreasts", true);
            Scribe_Values.Look(ref drawSizedApparelBreastsOnlyWorn, "drawSizedApparelBreastsOnlyWorn", false);
            Scribe_Values.Look(ref drawPenis, "drawPenis", true);
            Scribe_Values.Look(ref drawVagina, "drawVagina", true);
            Scribe_Values.Look(ref drawAnus, "drawAnus", true);
            Scribe_Values.Look(ref drawUdder, "drawUdder", true);
            Scribe_Values.Look(ref drawBelly, "drawBelly", true);


            /*
            Scribe_Values.Look(ref useTitanic, "useTitanic", true);
            Scribe_Values.Look(ref useColossal, "useColossal", true);
            Scribe_Values.Look(ref useGargantuan, "useGargantuan", true);
            Scribe_Values.Look(ref useMassive, "useMassive", true);
            Scribe_Values.Look(ref useEnormous, "useEnormous", true);
            Scribe_Values.Look(ref useHuge, "useHuge", true);
            Scribe_Values.Look(ref useLarge, "useLarge", true);
            Scribe_Values.Look(ref useAverage, "useAverage", true);
            Scribe_Values.Look(ref useSmall, "useSmall", true);
            Scribe_Values.Look(ref useTiny, "useTiny", true);
            Scribe_Values.Look(ref useNipples, "useNipples", true);
            */

            Scribe_Values.Look(ref useUnderBreasts, "useUnderBreasts",true);
            Scribe_Values.Look(ref UnderBreastsOffset, "UnderBreastsOffset", -0.0013f);
            base.ExposeData();
        }

    }

    public class SizedApparelMod : Mod
    {

        SizedApparelSettings settings;
        private static Vector2 ScrollPos = Vector2.zero;

        public SizedApparelMod(ModContentPack content): base(content)
        {
            this.settings = GetSettings<SizedApparelSettings>();
        }

        public override void DoSettingsWindowContents(Rect inRect)
        {
            Listing_Standard listingStandard = new Listing_Standard();
            //Rect rect = new Rect(0f, 0f, inRect.width, 950);
            //Rect rect = inRect.ExpandedBy(0.9f);
            Rect leftRect = new Rect(inRect.position, new Vector2(inRect.width / 2, inRect.height));
            Rect rightRect = new Rect(inRect.position + new Vector2(inRect.width / 2,0), new Vector2(inRect.width / 2, inRect.height));
            //rect.xMax *= 0.9f;
            leftRect = leftRect.ExpandedBy(0.9f);
            rightRect = rightRect.ExpandedBy(0.9f);
            listingStandard.Begin(leftRect);

            listingStandard.CheckboxLabeled("Debug Log", ref SizedApparelSettings.Debug, "Debug logs.\nDefault: false");
            if (SizedApparelSettings.Debug)
            {
                listingStandard.CheckboxLabeled("Debug Log (Detail)", ref SizedApparelSettings.DetailLog, "Debug logs for checking missing textures.\nDefault: false");
                if (listingStandard.ButtonTextLabeled("Clear Cache", "do not push unless you really need"))
                {
                    SizedApparelsDatabase.ClearAll();
                }
            }
            else
                listingStandard.Gap();

            listingStandard.Gap(8);
            listingStandard.Label("Optimization",-1,"");
            //listingStandard.CheckboxLabeled("Debug Log", ref SizedApparelSettings.Debug, "Debug logs.\nDefault: false");
            listingStandard.Gap(8);

            listingStandard.CheckboxLabeled("Use Breast Size Cap from Apparels(recommended: true)", ref SizedApparelSettings.useBreastSizeCapForApparels, "unite breast size from apparels. Smallest value will be used.\nIf you change this option, you need to redress Pawn\nDefault: true");


            //listingStandard.BeginScrollView(rect, ref ScrollPos, ref rect);
            //listingStandard.Label("Body(Torso)Texture Option", -1, "");
            //listingStandard.CheckboxLabeled("use Body (Torso) Texture", ref SizedApparelSettings.useBodyTexture, "change body texture if the pawn is wearing supported apparels only.\nIt may override bodytexture you use.\nIf you use rimnudeworld, don't use this option.\nDefault: false");
            //if (SizedApparelSettings.useBodyTexture == true)
            //{
            //    listingStandard.CheckboxLabeled("  use unsupportedApparel Body Texture", ref SizedApparelSettings.useUnsupportedBodyTexture, "Use \"_UnsupportedApparel\" Body Texture when pawn is wearing unsupported apparel.\nIf this option is false, the body will be default texture.\nDefault: true");
            //    listingStandard.CheckboxLabeled("  Match BodyTexture To Minimum ApparelSize", ref SizedApparelSettings.matchBodyTextureToMinimumApparelSize, "Avoid Clipping When breasts bigger than supported sized apparel.\nDefault: true");
            //
            //}
            listingStandard.Gap(8);
            listingStandard.Label("Other Mod Compatibility");
            listingStandard.CheckboxLabeled("  Unsupported Humanlike race use \"Humanlike\" bodyparts textures.", ref SizedApparelSettings.UnsupportedRaceToUseHumanlike, "If unchecked, unsupported humanlike race will not be patched!\nIf you change this option, you need to restart rimworld\nDefault: true");

            /*
            if(SizedApparelPatch.DubsApparelTweaksActive == true)
            {
                listingStandard.Label("   Dubs Apparel Tweaks Patched! (may not work in 1.3)");
                listingStandard.Gap(8);
            }*/

            //sizeList.EndScrollView(ref rect);
            //listingStandard.EndSection(sizeList);
            //listingStandard.EndScrollView(ref sizeScrollRect);
            listingStandard.Label("Non RimJobWorld Compatibility (wip)", -1, "User Who play without RimJobWorld");
            if (!SizedApparelPatch.RJWActive)
            {
                if (SizedApparelPatch.SJWActive)
                {
                    listingStandard.Label("  SafeJobWorld is Actived ", -1, "");
                    listingStandard.CheckboxLabeled("     use SafeJobWorld's Breasts(Hidden to player but it exist)", ref SizedApparelSettings.useSafeJobBreasts, "use BreastsSize from SJW.\nDefault: true");

                }
                if(SizedApparelPatch.SJWActive? SizedApparelSettings.useSafeJobBreasts==false : true)
                {
                    listingStandard.CheckboxLabeled("       use Random Breasts Size(not yet work)", ref SizedApparelSettings.useRandomSize, "use breasts random size for pawn.\nDefault: true");
                    listingStandard.Label("     random Size Min: " + SizedApparelSettings.UnderBreastsOffset.ToString(), -1, "Defualt: 0.01");
                    SizedApparelSettings.randomSizeMin = listingStandard.Slider(SizedApparelSettings.randomSizeMin, 0f, 2f);
                    listingStandard.Label("     random Size Max: " + SizedApparelSettings.UnderBreastsOffset.ToString(), -1, "Defualt: 1.00");
                    SizedApparelSettings.randomSizeMax = listingStandard.Slider(SizedApparelSettings.randomSizeMax, SizedApparelSettings.randomSizeMin, 2f);

                }
                if (SizedApparelPatch.SJWActive == false)
                {


                }
            }
            else
            {
                listingStandard.Label("  RimJobWorld is Actived ", -1, "");
            }
            
            listingStandard.Gap(12);
            listingStandard.Label("AlienRace Compatibility (wip)", -1, "");
            if (SizedApparelPatch.alienRaceActive)
            {
                listingStandard.Label("  AlienRace is Actived ", -1, "");
                //listingStandard.CheckboxLabeled("Force to use Human's BodyParts for unsuported Alien races", null, "");
            }
            else
            {
                listingStandard.Label("  AlienRace is not Actived ", -1, "");
            }
            

            listingStandard.Gap(16);
            listingStandard.Label("RimNudeWorld Compatibility (WIP)", -1, "");

            //listingStandard.CheckboxLabeled("  Don't Replace Body Texture On Nude", ref SizedApparelSettings.DontReplaceBodyTextureOnNude, "Only Replace BodyTexture On Not Nude. Trigers are Torso And Chests.\nDefault: False");
            if (SizedApparelPatch.rimNudeWorldActive)
            {
                listingStandard.Label("  RimNudeWorld is On!. Please check SizedApparel's Body parts render option", -1, "disable all body parts render except the breasts. and set it to only worn option true");
                listingStandard.CheckboxLabeled(" force to Draw SizedApparel's Breasts when worn (only non nude).", ref SizedApparelSettings.drawSizedApparelBreastsOnlyWorn, "use Sized Apparel's breasts render for breasts of apparel. it will be hidden when the pawn is naked and rimnudeworld will handle nude. \ndefault = true;");
                //listingStandard.CheckboxLabeled("  Hide RimNudeWorld Breasts Addon", ref SizedApparelSettings.hideRimNudeWorldBreasts, "For User Who Use Body(Torso) Texture option, remove double drawn breasts.\nYou can use this option as only using Rimnudeworld genital and else without breasts.\nDefault: False");
                if (false)//SizedApparelSettings.hideRimNudeWorldBreasts == false
                {
                    //listingStandard.CheckboxLabeled("  match Breast Texture To Minimum ApparelSize (not work)", ref SizedApparelSettings.matchBreastTextureToMinimumApparelSize, "Avoid Clipping When breasts bigger than supported sized apparel.\nDefault: true");

                    //listingStandard.CheckboxLabeled("  use Under Breasts addon (RimNudeWorld)(not recomanded)", ref SizedApparelSettings.useUnderBreasts, "draw breasts under apparel.");
                    listingStandard.Label("  Under Breasts Offset: " + SizedApparelSettings.UnderBreastsOffset.ToString(), -1, "offset from defeault layer offset. Defualt: -0.0013");
                    SizedApparelSettings.UnderBreastsOffset = listingStandard.Slider(SizedApparelSettings.UnderBreastsOffset, -0.025f, 0.025f);
                }
                if (SizedApparelSettings.useBodyTexture)
                {

                }
            }
            else
            {
                listingStandard.Label("  RimNudeWorld is not Actived ", -1, "");

            }
            listingStandard.End();


            listingStandard.Begin(rightRect);
            listingStandard.Label("If you changed the option, try change apparels or reload save", -1);
            listingStandard.Label("Body Part Render Option (wip)",-1,"standalone BodyPart Render System from this mod. It's for user who don't use RimNudeWorld\nIf you use RimNudeWorld, you should turn off this.");
            listingStandard.CheckboxLabeled("Draw Body Parts", ref SizedApparelSettings.drawBodyParts, "Draw Breasts..etc. when the pawn is wearing supported apparels. \nDefault: true");
            if (SizedApparelSettings.drawBodyParts)
            {
                listingStandard.CheckboxLabeled(" Use (Sized Apparel) Body Texture", ref SizedApparelSettings.useBodyTexture, "change pawn's body texture when the pawn is wearing supported apparels. Recommanded\nDefault: true");

                listingStandard.CheckboxLabeled(" Draw Muscle Overlay (wip)", ref SizedApparelSettings.drawMuscleOverlay, "\nDisable this option when you use RimNudeWorld");

                listingStandard.CheckboxLabeled(" Draw Breasts", ref SizedApparelSettings.drawBreasts, "this option is why this mod exist.\nDefault: true");
                if (SizedApparelSettings.drawBreasts)
                {
                    listingStandard.CheckboxLabeled("   Match Breasts size to supported apparels",ref SizedApparelSettings.matchBreastToSupportedApparelSize, "to avoid breasts clipping(when breasts are bigger), you need this option.\nDefault: true");
                    listingStandard.CheckboxLabeled("   draw Breasts on worn pawn only (RimNudeWorld)", ref SizedApparelSettings.drawSizedApparelBreastsOnlyWorn, "when the pawn is nude, the breasts graphic for sized apparel will be hidden. \nDefault: false" );
                }
                listingStandard.CheckboxLabeled(" Draw Penis", ref SizedApparelSettings.drawPenis,"Disable this option when you use RimNudeWorld");
                listingStandard.CheckboxLabeled(" Draw Vagina", ref SizedApparelSettings.drawVagina, "Disable this option when you use RimNudeWorld");
                listingStandard.CheckboxLabeled(" Draw Anus", ref SizedApparelSettings.drawAnus, "Disable this option when you use RimNudeWorld");
                listingStandard.CheckboxLabeled(" Draw Belly Buldge", ref SizedApparelSettings.drawBelly, "Disable this option when you use RimNudeWorld");

                listingStandard.CheckboxLabeled(" Hide Balls of Futa", ref SizedApparelSettings.hideBallOfFuta, "Hide Balls from penis of Futa.\nDefault: false");
                listingStandard.CheckboxLabeled(" Hide Penis of Man(Not Work yet)", ref SizedApparelSettings.hidePenisOfMale, "this option is for someone who really hate to see male's dick around.\nDefault: false");
            }

            /*
            listingStandard.Gap(4);
            listingStandard.Label("Breast Size Toggle Option", -1, "default option is setted for RimnudeWorld. you should not change this unless you have the textures for that size.");
            listingStandard.Gap(4);
            //Rect sizeScrollRect = new Rect(inRect.position+ new Vector2(0, listingStandard.CurHeight), inRect.size/3);
            //Vector2 sizeScrollPosition = new Vector2(0.9f, 0.5f);
            //listingStandard.BeginScrollView(sizeScrollRect, ref sizeScrollPosition, ref sizeScrollRect);
            //Listing_Standard sizeList = listingStandard.BeginSection_NewTemp(150);

            //sizeList.BeginScrollView(rect, ref ScrollPos, ref rect);
            
            listingStandard.CheckboxLabeled("  use Nipples", ref SizedApparelSettings.useNipples, "use Nipples(Flat breasts) Sized Apparel.\nDefault: true");
            listingStandard.CheckboxLabeled("  use Tiny", ref SizedApparelSettings.useTiny, "use Tiny breasts Sized Apparel.\nDefault: true");
            listingStandard.CheckboxLabeled("  use Small", ref SizedApparelSettings.useSmall, "use Small breasts Sized Apparel.\nDefault: true");
            listingStandard.CheckboxLabeled("  use Average", ref SizedApparelSettings.useAverage, "use Average breasts Sized Apparel.\nDefault: true");
            listingStandard.CheckboxLabeled("  use Large", ref SizedApparelSettings.useLarge, "use Large breasts Sized Apparel.\nDefault: true");
            listingStandard.CheckboxLabeled("  use Huge", ref SizedApparelSettings.useHuge, "use Huge breasts Sized Apparel.\nDefault: true");
            listingStandard.CheckboxLabeled("  use Enormous", ref SizedApparelSettings.useEnormous, "use Enormous breasts Sized Apparel.\nDefault: true");
            listingStandard.CheckboxLabeled("  use Massive", ref SizedApparelSettings.useMassive, "use Massive breasts Sized Apparel.\nDefault: true");
            listingStandard.CheckboxLabeled("  use Gargantuan", ref SizedApparelSettings.useGargantuan, "use Gargantuan breasts Sized Apparel.\nDefault: true");
            listingStandard.CheckboxLabeled("  use Colossal", ref SizedApparelSettings.useColossal, "use Colossal breasts Sized Apparel.\nDefault: true");
            listingStandard.CheckboxLabeled("  use Titanic", ref SizedApparelSettings.useTitanic, "use Titanic breasts Sized Apparel.\nDefault: true");
            listingStandard.End();
            */


            //listingStandard.EndScrollView(ref rect);
            base.DoSettingsWindowContents(inRect);
        }

        public override string SettingsCategory()
        {
            return "Sized Apparel";
        }

    }
}
