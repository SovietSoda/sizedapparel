﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RimWorld;
using Verse;
using HarmonyLib;
using AlienRace;
using UnityEngine;
using rjw;
using System.Reflection;
using System.Reflection.Emit;

namespace SizedApparel
{
    //TODO...?
    [HarmonyPatch(typeof(Dialog_StylingStation), "DrawTabs")]
    public class SizedApparelStyleStationDrawTabsPatch
    {
        public static void Postfix(Rect rect, ref Vector2 ___hairScrollPosition, Dialog_StylingStation __instance, List<TabRecord> ___tabs, Dialog_StylingStation.StylingTab ___curTab, float ___viewRectHeight, List<StyleItemDef> ___tmpStyleItems, bool ___devEditMode, Pawn ___pawn)
        {

            switch (___curTab)
            {
                case (Dialog_StylingStation.StylingTab)24:

                    //Draw PubicHair Tab Code here!

                    return;
                default:
                    return;
            }
        }

        static void DrawStylingTypePubicHair(Dialog_StylingStation dialog_StylingStation, Rect rect)
        {

        }

        static void AddPubicHairTab(Dialog_StylingStation stylingStation, List<TabRecord> tabs)
        {
            var curTabField = AccessTools.Field(typeof(Dialog_StylingStation), "curTab");
            tabs.Add(new TabRecord("PubicHair".Translate().CapitalizeFirst(), delegate ()
            {

                curTabField.SetValue(stylingStation, (Dialog_StylingStation.StylingTab)24);
            }, (Dialog_StylingStation.StylingTab)curTabField.GetValue(stylingStation) == (Dialog_StylingStation.StylingTab)24));
        }

        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        {
            bool isHair = false;
            MethodInfo tabAdd = AccessTools.DeclaredMethod(typeof(List<TabRecord>),"Add");
            foreach (var instruction in instructions)
            {
                if(instruction.opcode == OpCodes.Ldstr)
                {
                    if (instruction.OperandIs("Hair"))
                        isHair = true;
                    else
                        isHair = false;
                }


                if (isHair && instruction.opcode == OpCodes.Callvirt && instruction.OperandIs(tabAdd))
                {
                    yield return instruction;//finish add hairTab

                    yield return new CodeInstruction(OpCodes.Ldarg_0);
                    yield return new CodeInstruction(OpCodes.Ldarg_0);
                    Log.Message("this");
                    yield return new CodeInstruction(OpCodes.Ldfld, AccessTools.DeclaredField(typeof(Dialog_StylingStation),"tabs"));
                    Log.Message("tabs");
                    //yield return new CodeInstruction(OpCodes.Ldarg_0);
                    //Log.Message("this");
                    //yield return new CodeInstruction(OpCodes.Ldfld, AccessTools.DeclaredField(typeof(Dialog_StylingStation), "curTab"));
                    //Log.Message("curtab");
                    yield return new CodeInstruction(OpCodes.Call, AccessTools.Method(typeof(SizedApparelStyleStationDrawTabsPatch), "AddPubicHairTab"));
                    Log.Message("call");
                    //yield return new CodeInstruction(OpCodes.Ldarg_0);

                    //yield return new CodeInstruction(OpCodes.Ldarg_0);
                    //yield return new CodeInstruction(OpCodes.Ldfld, AccessTools.Field(typeof(Dialog_StylingStation), "tabs"));
                    //yield return new CodeInstruction(OpCodes.Ldstr,"PubicHair".Translate().CapitalizeFirst());

                    //yield return new CodeInstruction(OpCodes.Ldarg_0);
                    //yield return new CodeInstruction(OpCodes.Ldarg_0);




                    isHair = false;
                }
                else
                    yield return instruction;

            }
            yield break;
        }
    }
}

